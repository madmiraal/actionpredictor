// (c) 2016: Marcel Admiraal

#ifndef APLOGGER_H
#define APLOGGER_H

#include "imulogger.h"

class APLogger : public ma::IMULogger
{
public:
    /**
     * Constructor.
     *
     * @param filename  Name of the file to log to.
     */
    APLogger(const char* filename);

    /**
     * Default destructor.
     */
    virtual ~APLogger();

    /**
     * Logs that the current instruction changed.
     *
     * @param instruction The new instruction.
     */
    void currentInstruction(const char* instruction);

    /**
     * Logs that the predictor started learning.
     */
    void startLearning();

    /**
     * Logs that the predictor stopped learning.
     */
    void stopLearning();

    /**
     * Logs that the next state was sent to the predictor.
     */
    void sampleData();

    /**
     * Logs that the predictor stopped learning.
     */
    void sequenceReset();

    /**
     * Logs that the predictor received the actual result.
     *
     * @param resultIndex   The index of the actual result.
     */
    void actualResult(const unsigned int resultIndex);

    /**
     * Logs that IMU settings are being recorded.
     */
    void recordIMUSettings();

    /**
     * Logs that meta data is being recorded.
     */
    void recordingPredictionMetaData();

    /**
     * Logs that a prediction is being recorded.
     */
    void recordingPrediction();

    /**
     * Logs that a blank line was added to the predicition log file.
     */
    void addingBlankLineToPredicitionLogFile();
};

#endif // APLOGGER_H
