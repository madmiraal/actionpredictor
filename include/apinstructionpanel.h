// (c) 2016: Marcel Admiraal

#ifndef APINSTRUCTIONPANEL_H
#define APINSTRUCTIONPANEL_H

#include <wx/panel.h>

#include "apcontrol.h"

#include "fittextpanel.h"

class APInstructionPanel : public wxPanel
{
public:
    /**
     * Constructor.
     *
     * @param parent    Pointer to the panel's parent.
     * @param control   Pointer to the controller.
     */
    APInstructionPanel(wxWindow* parent, APControl* control);

    /**
     * Default destructor.
     */
    virtual ~APInstructionPanel();

private:
    // Intercept erase event function.
    void stopErase(wxEraseEvent& event);
    // Idle event function
    void onIdle(wxIdleEvent& event);

    APControl* control;
    ma::FitTextPanel* instructionPanel;
    ma::FitTextPanel* remainingPanel;
};

#endif // APINSTRUCTIONPANEL_H
