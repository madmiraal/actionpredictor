// (c) 2016: Marcel Admiraal

#ifndef APAPP_H
#define APAPP_H

#include <wx/app.h>

class APApp : public wxApp
{
public:
    /**
     * Initialises the application.
     *
     * @return True to continue processing.
     */
    virtual bool OnInit();
};

#endif // APAPP_H
