// (c) 2016 - 2019: Marcel Admiraal

#ifndef APFRAME_H
#define APFRAME_H

#include <wx/toolbar.h>
#include <wx/dialog.h>

#include "controlframe.h"

#include "apcontrol.h"

#include "vector.h"
#include "neuron.h"

class APFrame : public ma::ControlFrame
{
public:
    /**
     * Default constructor.
     */
    APFrame();

    /**
     * Default destructor.
     */
    virtual ~APFrame();

private:
    void setRecordedDataDirectory(wxCommandEvent& event);
    void setLearningMode(wxCommandEvent& event);
    void newLearner(wxCommandEvent& event);
    void loadLearner(wxCommandEvent& event);
    void saveLearner(wxCommandEvent& event);
    void setLearningRate(wxCommandEvent& event);
    void setDiscountRate(wxCommandEvent& event);
    void setRegularisationFactor(wxCommandEvent& event);
    void setLambda(wxCommandEvent& event);
    void setInputRate(wxCommandEvent& event);
    void setLearningInstancesPerInstruction(wxCommandEvent& event);
    void setLearningTranches(wxCommandEvent& event);
    void setLearningShowTime(wxCommandEvent& event);
    void setDisplayPackets(wxCommandEvent& event);
    void configureIMUs(wxCommandEvent& event);
    void loadInstructions(wxCommandEvent& event);
    void saveInstructions(wxCommandEvent& event);
    void editInstructions(wxCommandEvent& event);
    void setPlaybackFile(wxCommandEvent& event);
    void setRealtime(wxCommandEvent& event);
    void loopPlayback(wxCommandEvent& event);
    void setRecordFile(wxCommandEvent& event);
    void setPredictionLogFile(wxCommandEvent& event);
    void controlMouse(wxCommandEvent& event);
    void setMouseSensitivity(wxCommandEvent& event);
    void pauseLearningToolClicked(wxCommandEvent& event);
    void learnToolClicked(wxCommandEvent& event);
    void recordToolClicked(wxCommandEvent& event);
    void startPlaybackClicked(wxCommandEvent& event);
    void pausePlaybackClicked(wxCommandEvent& event);
    void stopToolClicked(wxCommandEvent& event);
    void quit(wxCommandEvent& event);

    APControl* apcontrol;

    wxToolBarToolBase* pauseLearnTool, * continueLearnTool;
    wxToolBarToolBase* learnTool, * predictTool;
    wxToolBarToolBase* playbackStartTool, * playbackPauseTool;
};

void getNetworkDetails(wxWindow* parent, ma::Vector& layerSize,
        ma::NeuronType& neuronType);
bool getHiddenLayerSizes(wxWindow* parent, ma::Vector& layerSize);
bool getHiddenNeuronType(wxWindow* parent, ma::NeuronType& neuronType);

#endif // APFRAME_H
