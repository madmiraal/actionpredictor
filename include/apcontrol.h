// (c) 2016 - 2019: Marcel Admiraal

#ifndef APCONTROL_H
#define APCONTROL_H

#include <wx/string.h>

#include "control.h"

#include "aplogger.h"
#include "aplogplayback.h"
#include "apmousecontrol.h"

#include "fileserial.h"
#include "imudatacollector.h"
#include "neuron.h"
#include "predictor.h"
#include "str.h"
#include "vector.h"

#include <chrono>
#include <fstream>

enum Timers
{
    SAMPLE_TIMER
};

enum class LearningState
{
    INITIALISE_LEARNING,
    INITIALISE_LEARNING_TRANCHE,
    INITIALISE_COUNTDOWN,
    COUNT_DOWN,
    INITIALISE_LEARNING_STEP,
    LEARNING,
    INITIALISE_TESTING,
    INITIALISE_TESTING_STEP,
    TESTING,
    DONE
};

enum class LearningMode
{
    INTERACTIVE,
    EMBEDDED_SIGNAL,
    EMBEDDED_SNAP
};

class APControl : public ma::Control
{
public:
    /**
     * Default constructor.
     */
    APControl();

    /**
     * Default destructor.
     */
    virtual ~APControl();

    /**
     * Sets whether or not to pause the learning tranches.
     *
     * @param learningPaused    Whether or not the learning should be paused.
     */
    void pauseLearning(bool learningPaused);

    /**
     * Returns whether or not the learning is paused.
     *
     * @return Whether or not the learning is paused.
     */
    bool isLearningPaused();

    /**
     * Sets whether or not to learn.
     *
     * @param learningPaused    Whether or not it should learn.
     */
    void learn(bool learning);

    /**
     * Returns whether or not it's learning.
     *
     * @return Whether or not it's learning.
     */
    bool isLearning();

    /**
     * Returns the current prediction.
     *
     * @return The current prediction.
     */
    ma::Vector getPrediction();

    /**
     * Returns a pointer to the data collector.
     *
     * @param withLock  Whether or not configuration access to the data
     *                  collector is required. Default = false.
     * @return          A pointer to the data collector.
     */
    ma::IMUDataCollector* getDataCollector(bool withLock = false);

    /**
     * Returns data collector access to the controller.
     */
    void returnDataCollector();

    /**
     * Returns the data sample period.
     *
     * @return The data sample period.
     */
    unsigned int getSamplePeriod();

    /**
     * Sets the new sample timer period.
     *
     * @param newPeriod The new sampling period.
     */
    void setSamplePeriod(const unsigned int newPeriod);

    /**
     * Returns the learning mode.
     *
     * @return The learning mode.
     */
    LearningMode getLearningMode() const;

    /**
     * Sets the learning mode.
     *
     * @param learningMode  The required learning mode.
     */
    void setLearningMode(const LearningMode learningMode);

    /**
     * Returns the directory holding the recorded data.
     *
     * @return The directory holding the recorded data.
     */
    wxString getRecordedDataDirectory() const;

    /**
     * Sets the directory holding the recorded data.
     *
     * @param The directory holding the recorded data.
     */
    void setRecordedDataDirectory(const wxString& recordedDataDirectory);

    /**
     * Returns whether or not the learner is a Net Learner.
     *
     * @return Whether or not the learner is a Net Learner.
     */
    bool isNetLearner() const;

    /**
     * Returns whether or not the learner is a TD Net Learner.
     *
     * @return Whether or not the learner is a TD Net Learner.
     */
    bool isTDNetLearner() const;

    /**
     * Creates a new MCMC learner for a player.
     */
    void newMCMCLearner();

    /**
     * Creates a new MC network learner for a player.
     *
     * @param layerSize The vector containing the network layers' sizes.
     */
    void newMCNetLearner(ma::Vector layerSize,
            const ma::NeuronType hiddenType);

    /**
     * Creates a new TD network learner for a player.
     *
     * @param layerSize The vector containing the network layers' sizes.
     */
    void newTDNetLearner(ma::Vector layerSize,
            const ma::NeuronType hiddenType);

    /**
     * Loads previously saved learner for a player.
     *
     * @param filename  The filename of the saved learning.
     */
    bool loadLearner(const wxString& filename);

    /**
     * Saves a player's learner.
     *
     * @param playerId  The id of the player.
     * @param filename  The filename to save the learning to.
     */
    bool saveLearner(const wxString& filename);

    /**
     * Returns the learning rate.
     *
     * @return The learning rate.
     */
    double getLearningRate();

    /**
     * Sets the learning rate.
     *
     * @param learningRate  The new learning rate.
     */
    void setLearningRate(const double learningRate);

    /**
     * Returns the discount rate.
     *
     * @return The discount rate.
     */
    double getDiscountRate();

    /**
     * Sets the discount rate.
     *
     * @param discountRate  The new discount rate.
     */
    void setDiscountRate(const double discountRate);

    /**
     * Returns the net learner's regularisation factor.
     *
     * @return The net learner's regulariasation factor.
     */
    double getRegularisationFactor();

    /**
     * Sets the net learner's regularisation factor.
     *
     * @param regularisationFactor  The new regularisation factor.
     */
    void setRegularisationFactor(const double regularisationFactor);

    /**
     * Returns the TD Net learner's prediction discount rate.
     *
     * @return The current prediction discount rate.
     */
    double getLambda();

    /**
     * Sets the TD Net learner's prediction discount rate.
     *
     * @param newRate   The new prediction discount rate.
     */
    void setLambda(const double newLambda);

    /**
     * Loads instructions from the specified file.
     *
     * @param filename  The filename containing the instructions.
     * @return          Whether or not the load was successful.
     */
    bool loadInstructions(const wxString& filename);

    /**
     * Saves instructions to the specified file.
     *
     * @param filename  The filename to save the instructions to.
     * @return          Whether or not the save was successful.
     */
    bool saveInstructions(const wxString& filename);

    /**
     * Returns the current instruction.
     *
     * @return The current instruction.
     */
    ma::Str getCurrentInstruction();

    /**
     * Sets the current instruction.
     *
     * @return The current instruction.
     */
    void setCurrentInstruction(const ma::Str& instruction);

    /**
     * Returns the number of instructions.
     *
     * @return The number of instructions.
     */
    unsigned int getInstructions() const;

    /**
     * Returns the instruction with the specified index.
     *
     * @param instructionIndex  The index of the instruction required.
     * @return                  The instruction with the specified index.
     */
    ma::Str getInstruction(const unsigned int instructionIndex);

    /**
     * Returns the instruction with the specified index.
     *
     * @param instructions  The string containing all the instructions separated
     *                      by newlines.
     */
    void setInstructions(const ma::Str& newInstructions);

    /**
     * Returns the learning instances remaining in this tranche.
     *
     * @return The learning instances remaining in this tranche.
     */
    unsigned int getLearningInstancesRemaining() const;

    /**
     * Returns the learning instances per instruction per tranche.
     *
     * @return The learning instances per instruction per tranche.
     */
    unsigned int getLearningInstancesPerInstruction() const;

    /**
     * Sets the learning instances per instruction per tranche.
     *
     * @param learningInstancesPerInstruction   The new learning instances
     *                                          per instruction per tranche.
     */
    void setLearningInstancesPerInstruction(
            const unsigned int learningInstancesPerInstruction);

    /**
     * Returns the learning tranches.
     *
     * @return The learning tranches.
     */
    unsigned int getLearningTranches() const;

    /**
     * Sets the learning tranches.
     *
     * @param learningTranches  The new learning tranches.
     */
    void setLearningTranches(const unsigned int learningTranches);

    /**
     * Returns the learning show time.
     *
     * @return The learning show time.
     */
    unsigned int getLearningShowTime() const;

    /**
     * Sets the learning show time.
     *
     * @param showTime  The new learning show time.
     */
    void setLearningShowTime(const unsigned int showTime);

    /**
     * Returns the recording log filename.
     *
     * @return The recording log filename.
     */
    wxString getRecordFilename();

    /**
     * Sets the recording log filename.
     *
     * @param filenamen The new recording log filename.
     */
    void setRecordFilename(const wxString& filename);

    /**
     * Starts recording.
     */
    void startRecording();

    /**
     * Stops logging.
     */
    void stopRecording();

    /**
     * Returns whether or not the logger is logging.
     *
     * @return Whether or not the logger is logging.
     */
    bool isRecording();

    /**
     * Set whether or not to display the packets received.
     *
     * @param display   Whether or not to display the packets received.
     */
    void displayPackets(bool display);

    /**
     * Sets whether or not to use real-time playback.
     *
     * @param realtime  Whether or not to use real-time playback.
     */
    void setRealtime(bool realTime = true);

    /**
     * Returns the playback log filename.
     *
     * @return The playback log filename.
     */
    wxString getPlaybackFilename();

    /**
     * Sets the playback log filename.
     *
     * @param filename The new playback log filename.
     */
    void setPlaybackFilename(const wxString& filename);

    /**
     * Starts playing back a recording.
     * or unpauses a previously paused playback.
     */
    void startPlayback();

    /**
     * Pauses a play back.
     * Start play back will continue the playback from when it was paused.
     */
    void pausePlayback();

    /**
     * Stops a play back.
     * Start play back will start the playback from the begining.
     */
    void stopPlayback();

    /**
     * Returns whether or not the playback is playing.
     *
     * @return Whether or not the playback is playing.
     */
    bool isPlaybackPlaying() const;

    /**
     * Returns whether or not the playback is paused.
     *
     * @return Whether or not the playback is paused.
     */
    bool isPlaybackPaused() const;

    /**
     * Returns whether or not the playback loops.
     *
     * @return Whether or not the playback loops.
     */
    bool isPlaybackLooping() const;

    /**
     * Sets whether or not to loop the playback.
     *
     * @param enable Whether or not to loop the playback.
     */
    void loopPlayback(const bool enable = true);

    /**
     * Returns the number of times the playback loops.
     * Note: -1 indicates the playback loops continuously.
     *
     * @return The number of times the playback loops.
     */
    int getLoopPlaybackCount() const;

    /**
     * Sets the number of times the playback loops.
     * Note: Use -1 to indicate that the playback should loop continuously.
     *
     * @param loopCount The number of times the playback loops.
     */
    void setLoopPlaybackCount(const int loopCount);

    /**
     * Sets the predictor, but not the controller to start learning.
     */
    void startPredictorLearning();

    /**
     * Sets the predictor, but not the controller to stop learning.
     */
    void stopPredictorLearning();

    /**
     * Informs predictor to sample the data.
     */
    void tellPredictorToSampleData();

    /**
     * Informs predictor to reset sequence without final result.
     */
    void tellPredictorSequenceReset();

    /**
     * Informs the predictor, of the actual result.
     *
     * @param instructionIndex  The index of the correct result.
     */
    void tellPredictorActualResult(unsigned int instructionIndex);

    /**
     * Returns the prediction log filename.
     *
     * @return The prediction log filename.
     */
    wxString getPredictionLogFilename() const;

    /**
     * Sets the prediction log filename.
     *
     * @param filename The new prediciton log filename.
     */
    void setPredictionLogFilename(const wxString& filename);

    /**
     * Records the current Learner Settings.
     */
    void recordLearnerSettings();

    /**
     * Records the current IMU Settings.
     */
    void recordIMUSettings(bool changed = false);

    /**
     * Records the current prediction meta data.
     */
    void recordPredictionMetaData();

    /**
     * Records the current prediction.
     */
    void recordPrediction();

    /**
     * Creates a blank line in the record.
     */
    void addBlankLineToPredicitionLogFile();

    /**
     * Sets whether or not to control the mouse cursor.
     *
     * @param controllingMouse Whether or not to control the mouse cursor.
     */
    void controlMouse(bool controllingMouse);

    /**
     * Returns the current mouse control sensitivity.
     *
     * @return The current mouse control sensitivity.
     */
    unsigned int getMouseSensitivity() const;

    /**
     * Sets the mouse control sensitivity.
     *
     * @param milliSecondDelay  The mouse control sensitivity.
     */
    void setMouseSensitivity(const unsigned int milliSecondDelay);

private:
    // Override timer function.
    virtual void timerFunction(const unsigned int id);
    // Override loop function.
    virtual void loopFunction();
    void initialiseLearning();
    void initialiseLearningTranche();
    void initialiseEmbeddedTranche();
    void initialiseCountDown();
    void displayCountDown();
    void initialiseLearningStep();
    void initialiseInteractiveStep();
    void initialiseEmbeddedStep();
    void displayLearning();
    void displayInteractiveLearning();
    void displayEmbeddedLearning();
    void displayEmbeddedSnapLearning();
    void initialiseTesting();
    void initialiseTestingStep();
    void displayTesting();
    void displayInteractiveTesting();
    void displayEmbeddedTesting();
    void displayEmbeddedSnapTesting();

    unsigned int getNextInstructionIndex();

    void sampleData();
    void setInteractiveLearningValues();
    void setLearningValuesFromGestureTrancheFilenames();
    void clearFileSerial();

    ma::FileSerial* initialFileSerial;
    ma::FileSerial** fileSerial;
    unsigned int fileSerialUsed;
    bool learnerSettingsChanged;

    // Access control mutices.
    wxMutex dataCollectorAccess;
    wxMutex predictorAccess;
    wxMutex instructionAccess;
    wxMutex recordedDataAccess;

    // Learning control.
    LearningState learningState;
    LearningMode learningMode;
    unsigned int gestures;
    bool learning;
    bool signal;
    bool useNoSignal;

    // Learning timers.
    unsigned int countDown, showTimeRemaining, showTime;
    unsigned int learningInstancesRemaining;
    unsigned int learningInstancesPerInstruction;
    unsigned int* learningInstructionUsed;
    unsigned int currentLearningTranche;
    unsigned int learningTranches;
    unsigned int averageSampleLength;
    std::chrono::milliseconds sampleStarted;
    unsigned int triggerPoint;
    bool predictionRecorded;

    // Instruction variables.
    unsigned int instructionIndex;
    unsigned int instructions;
    ma::Str currentInstruction;
    ma::Str* instructionText;

    // IMU data collection.
    ma::IMUDataCollector dataCollector;

    // Recorded data.
    unsigned int gestureFiles;
    ma::Str recordedDataDirectory;

    // Record predictions variables.
    unsigned int predictionIndex;
    unsigned int predictorTranches;
    unsigned int predictorSamples;
    ma::Str predictionLogFilename;
    std::fstream predictionLogFile;

    // Mouse control variables.
    bool controllingMouse;
    MouseControl mouse;

    // Components.
    // The predictor.
    ma::Predictor predictor;
    // The logger.
    APLogger* logger;
    // The log playback.
    APLogPlayback* playback;
};

#endif // APCONTROL_H

