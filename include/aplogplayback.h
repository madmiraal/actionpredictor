// (c) 2016: Marcel Admiraal

#ifndef APLOGPLAYBACK_H
#define APLOGPLAYBACK_H

#include "imulogplayback.h"

class APControl;

class APLogPlayback : public ma::IMULogPlayback
{
public:
    /**
     * Constructor.
     *
     * @param control   Pointer to the controller.
     * @param filename  The filename of the log file to use.
     */
    APLogPlayback(APControl* control, const ma::Str& filename);

    /**
     * Default destructor.
     */
    virtual ~APLogPlayback();

    /**
     * Starts or continues log playback.
     */
    void startPlayback();

    /**
     * Pauses log playback.
     * Start play back will continue the playback from when it was paused.
     */
    void pausePlayback();

    /**
     * Stops log playback.
     * Start play back will start the playback from the begining.
     */
    void stopPlayback();

protected:
    /**
     * Processes the log entry, and returns whether or not the log entry was
     * processed.
     * Note: When overriding this function, call the parent function first,
     * and only process the entry if the parent function returns false i.e.
     * it didn't process the entry.
     *
     * @param entry     The entry to process.
     * @return          Whether or not the log entry was processed.
     */
    virtual bool processEntry(const ma::Str& entry);

private:
    // Overridden parent functions.
    void logFileStart();
    void logFileEnd();

    void currentInstruction(const ma::Str& instruction);
    void startLearning();
    void stopLearning();
    void sampleData();
    void sequenceReset();
    void actualResult(const ma::Str& resultString);
    void recordIMUSettings();
    void recordPredictionMetaData();
    void recordPrediction();
    void recordDelayedPrediction();
    void addBlankLineToPredicitionLogFile();

    APControl* control;
};

#endif // APLOGPLAYBACK_H
