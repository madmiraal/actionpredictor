// (c) 2016: Marcel Admiraal

#ifndef APMOUSECONTROL_H
#define APMOUSECONTROL_H

#include <wx/thread.h>

#include "pointer.h"

class APControl;

class MouseControl : wxThreadHelper
{
public:
    /**
     * Constructor
     *
     * @param control   The controller.
     */
    MouseControl(APControl* control);
    /**
     * Default destructor.
     */
    virtual ~MouseControl();

    /**
     * Returns the current mouse sensitivity.
     *
     * @return The current mouse sensitivity.
     */
    unsigned int getSensitivity() const;

    /**
     * Sets the mouse sensitivity.
     *
     * @param milliSecondDelay The number of milliseconds between updates.
     */
    void setSensitivity(const unsigned int milliSecondDelay);

    /**
     * Sets whether or not to control the mouse.
     *
     * @param controllingMouse  Whether or not to control the mouse.
     */
    void controlMouse(bool controllingMouse);

private:
    virtual wxThread::ExitCode Entry();

    unsigned int milliSecondDelay;
    bool running;
    bool controllingMouse;

    ma::Pointer pointer;
    APControl* control;
};

#endif // APMOUSECONTROL_H
