// (c) 2016: Marcel Admiraal

#ifndef APPREDICTIONPANEL_H
#define APPREDICTIONPANEL_H

#include <wx/panel.h>

#include "apcontrol.h"

class APPredictionPanel : public wxPanel
{
public:
    /**
     * Constructor
     *
     * @param parent    Pointer to the panel's parent.
     * @param control   Pointer to the controller.
     */
    APPredictionPanel(wxWindow* parent, APControl* control);

    /**
     * Default destructor.
     */
    virtual ~APPredictionPanel();

private:
    // Stop erase function
    void stopErase(wxEraseEvent& event);
    // Idle event function.
    void onIdle(wxIdleEvent& evt);
    // Paint event function
    void paint(wxPaintEvent& event);

    void setBackgroundColour(unsigned int& red, unsigned int& green,
            unsigned int& blue, const double confidence);

    APControl* control;
};

#endif // APPREDICTIONPANEL_H
