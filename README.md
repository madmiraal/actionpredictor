# Action Predictor #

An action predictor programme.
Takes input from multiple sensors: orientation quaternions, accelerometers, gyroscopes, magnetometers and any normalised analogue input and learns a list of actions.

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
* [MAANN](https://bitbucket.org/madmiraal/maann)
* [MAIMU](https://bitbucket.org/madmiraal/maimu)
* [MADSP](https://bitbucket.org/madmiraal/madsp)
* [MAIMUData](https://bitbucket.org/madmiraal/maimudata)