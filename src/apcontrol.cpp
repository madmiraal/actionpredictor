// (c) 2016 - 2019: Marcel Admiraal

#include "apcontrol.h"

#include <wx/progdlg.h>

#include "strstream.h"

#include <cstdlib>
#include <fstream>
#include <ctime>
#include <iomanip>

#define DEFAULT_SAMPLE_PERIOD 100 //milliseconds.
#define DEFAULT_LEARNING_INSTANCES_PER_INSTRUCTION 5
#define DEFAULT_LEARNING_TRANCHES 5
#define COUNTDOWN_SECONDS 3

APControl::APControl() : ma::Control(),
        // Initialise fileSerial pointer array.
        initialFileSerial(0), fileSerial(0), fileSerialUsed(0),
        learnerSettingsChanged(true),
        // Initialise learning control variables.
        learningState(LearningState::DONE), learningMode(LearningMode::INTERACTIVE),
        gestures(0), learning(false), signal(false), useNoSignal(false),
        // Initialise learning timers.
        countDown(0), showTimeRemaining(0), showTime(2),
        learningInstancesRemaining(0),
        learningInstancesPerInstruction(DEFAULT_LEARNING_INSTANCES_PER_INSTRUCTION),
        learningInstructionUsed(0), currentLearningTranche(0),
        learningTranches(DEFAULT_LEARNING_TRANCHES),
        averageSampleLength(0), triggerPoint(0), predictionRecorded(false),
        // Initialise instruction variables.
        instructionIndex(0), instructions(0), instructionText(0),
        gestureFiles(0),
        // Initialise prediction record variables.
        predictionIndex(0), predictorTranches(0), predictorSamples(0),
        predictionLogFilename("prediction.log"),
        // Initialise mouse control variables.
        controllingMouse(false), mouse(this)
{
    // Create components.
    logger = new APLogger("input.log");
    dataCollector.setLogger(logger);
    playback = new APLogPlayback(this, "input.log");

    // Load instructions.
    loadInstructions(wxT("Instructions.txt"));
    startTimer(SAMPLE_TIMER, DEFAULT_SAMPLE_PERIOD);

    // Set default predictor to TD Learner.
    predictor.newTDNetLearner();

    pauseLoop(false);
}

APControl::~APControl()
{
    // Clean up.
    delete playback;
    clearFileSerial();
    if (learningInstructionUsed) delete[] learningInstructionUsed;
    if (instructionText) delete[] instructionText;
}

void APControl::pauseLearning(bool learningPaused)
{
    logger->pauseLogging();
    // If learning paused and switching to not paused.
    if (isLoopPaused() && !learningPaused &&
        LearningState::DONE != learningState &&
        LearningState::INITIALISE_LEARNING != learningState)
    {
        learningState = LearningState::INITIALISE_LEARNING_TRANCHE;
    }
    else if (!isLoopPaused() && learningPaused)
    {
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::Str();
    }
    pauseLoop(learningPaused);
}

bool APControl::isLearningPaused()
{
    return isLoopPaused();
}

void APControl::learn(bool learning)
{
    if (learning)
    {
        learningState = LearningState::INITIALISE_LEARNING;
    }
    else
    {
        stopPredictorLearning();
        logger->stopLearning();
        logger->pauseLogging();
        learningState = LearningState::DONE;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::Str();
    }
    this->learning = learning;
}

bool APControl::isLearning()
{
    return learning;
}

ma::Vector APControl::getPrediction()
{
    wxMutexLocker dataCollectorLock(dataCollectorAccess);
    wxMutexLocker predictorLock(predictorAccess);
    return ma::normalProbability(
        predictor.getPrediction(dataCollector.getAllValues()));
}

ma::IMUDataCollector* APControl::getDataCollector(bool withLock)
{
    clearFileSerial();
    if (withLock) dataCollectorAccess.Lock();
    return &dataCollector;
}

void APControl::returnDataCollector()
{
    if (predictor.getStateLength() != dataCollector.getAllValuesSize())
    {
        wxMutexLocker predictorLock(predictorAccess);
        predictor.adjustPredictor(dataCollector.getAllValuesSize(),
                instructions);
    }
    dataCollectorAccess.Unlock();
}

unsigned int APControl::getSamplePeriod()
{
    return getTimerPeriod(SAMPLE_TIMER);
}

void APControl::setSamplePeriod(const unsigned int newPeriod)
{
    startTimer(SAMPLE_TIMER, newPeriod);
    learnerSettingsChanged = true;
}

LearningMode APControl::getLearningMode() const
{
    return learningMode;
}

void APControl::setLearningMode(const LearningMode learningMode)
{
    this->learningMode = learningMode;
    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
        setInteractiveLearningValues();
        startTimer(SAMPLE_TIMER);
        break;
    case LearningMode::EMBEDDED_SIGNAL:
        setLearningValuesFromGestureTrancheFilenames();
        startTimer(SAMPLE_TIMER);
        break;
    case LearningMode::EMBEDDED_SNAP:
        stopTimer(SAMPLE_TIMER);
        setLearningValuesFromGestureTrancheFilenames();
        ma::IMUDataCollector* data = getDataCollector(true);
        //data->setQuaternionHistoryLength(1000);
        //data->setAccelerometerHistoryLength(1000);
        //data->setGyroscopeHistoryLength(1000);
        //data->setMagnetometerHistoryLength(1000);
        data->setAnalogueHistoryLength(1000);
        returnDataCollector();
        break;
    }
}

wxString APControl::getRecordedDataDirectory() const
{
    return wxString(recordedDataDirectory.c_str());
}

void APControl::setRecordedDataDirectory(const wxString& recordedDataDirectory)
{
    wxMutexLocker recordedDataLock(recordedDataAccess);
    this->recordedDataDirectory = ma::Str(recordedDataDirectory.c_str()) + "/";
    if (LearningMode::EMBEDDED_SIGNAL == learningMode)
        setLearningValuesFromGestureTrancheFilenames();
}

bool APControl::isNetLearner() const
{
    return !predictor.isLearnerType(ma::LearnerType::MCMCLearner);
}

bool APControl::isTDNetLearner() const
{
    return predictor.isLearnerType(ma::LearnerType::TDNetLearner);
}

void APControl::newMCMCLearner()
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.newMCMCLearner(
            dataCollector.getAllValuesSize(), instructions);
    predictorTranches = 0;
    predictorSamples = 0;
    learnerSettingsChanged = true;
}

void APControl::newMCNetLearner(ma::Vector layerSize,
        const ma::NeuronType hiddenType)
{
    if (layerSize.size() > 1)
    {
        layerSize[0] = dataCollector.getAllValuesSize();
        layerSize[layerSize.size()-1] = instructions;
    }
    wxMutexLocker predictorLock(predictorAccess);
    predictor.newMCNetLearner(layerSize, hiddenType, predictor.getLearningRate(),
    predictor.getDiscountRate(), predictor.getRegularisationFactor());
    predictorTranches = 0;
    predictorSamples = 0;
    learnerSettingsChanged = true;
}

void APControl::newTDNetLearner(ma::Vector layerSize,
        const ma::NeuronType hiddenType)
{
    if (layerSize.size() > 1)
    {
        layerSize[0] = dataCollector.getAllValuesSize();
        layerSize[layerSize.size()-1] = instructions;
    }
    wxMutexLocker predictorLock(predictorAccess);
    predictor.newTDNetLearner(layerSize, hiddenType, predictor.getLearningRate(),
    predictor.getDiscountRate(), predictor.getRegularisationFactor(),
    predictor.getLambda());
    predictorTranches = 0;
    predictorSamples = 0;
    learnerSettingsChanged = true;
}

bool APControl::loadLearner(const wxString& filename)
{
    wxMutexLocker predictorLock(predictorAccess);
    bool success = predictor.loadLearner(filename.c_str());
    if (success)
    {
        predictorTranches = 0;
        predictorSamples = 0;
        predictionLogFile.open(predictionLogFilename.c_str(),
                std::fstream::out | std::fstream::app);
        if (predictionLogFile.is_open())
        {
            predictionLogFile << "\nLoaded Learner " << filename << std::endl;
            predictionLogFile.close();
        }
    }
    learnerSettingsChanged = true;
    return success;
}

bool APControl::saveLearner(const wxString& filename)
{
    wxMutexLocker predictorLock(predictorAccess);
    bool success = predictor.saveLearner(filename.c_str());
    if (success)
    {
        predictionLogFile.open(predictionLogFilename.c_str(),
                std::fstream::out | std::fstream::app);
        if (predictionLogFile.is_open())
        {
            predictionLogFile << "\nSaved Learner " << filename;
            predictionLogFile << " with " << predictorTranches;
            predictionLogFile << "(" << predictorSamples << ")";
            predictionLogFile << " Tranches(Total Samples)" << std::endl;
            predictionLogFile.close();
        }
    }
    return success;
}

double APControl::getLearningRate()
{
    wxMutexLocker predictorLock(predictorAccess);
    return predictor.getLearningRate();
}

void APControl::setLearningRate(const double learningRate)
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.setLearningRate(learningRate);
    learnerSettingsChanged = true;
}

double APControl::getDiscountRate()
{
    wxMutexLocker predictorLock(predictorAccess);
    return predictor.getDiscountRate();
}

void APControl::setDiscountRate(const double discountRate)
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.setDiscountRate(discountRate);
    learnerSettingsChanged = true;
}

double APControl::getRegularisationFactor()
{
    wxMutexLocker predictorLock(predictorAccess);
    return predictor.getRegularisationFactor();
}

void APControl::setRegularisationFactor(const double regularisationFactor)
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.setRegularisationFactor(regularisationFactor);
    learnerSettingsChanged = true;
}

double APControl::getLambda()
{
    wxMutexLocker predictorLock(predictorAccess);
    return predictor.getLambda();
}

void APControl::setLambda(const double newLambda)
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.setLambda(newLambda);
    learnerSettingsChanged = true;
}

bool APControl::loadInstructions(const wxString& filename)
{
    std::ifstream fileStream(filename.c_str(), std::ifstream::in);
    if (!fileStream.is_open()) return false;
    ma::Str line;
    if (!ma::getNext(fileStream, line)) return false;
    if (line != "Instructions:") return false;

    wxMutexLocker instructionLock(instructionAccess);
    // Count instructions.
    instructions = 0;
    while(ma::getNext(fileStream, line)) ++instructions;
    if (instructionText != 0) delete[] instructionText;
    instructionText = new ma::Str[instructions];
    // Go back to the beginning of the file.
    fileStream.clear();
    fileStream.seekg(0, std::ios::beg);
    // Ignore instruction line.
    line = ma::getNext(fileStream);
    for (unsigned int i = 0; i < instructions; ++i)
    {
        line = ma::getNext(fileStream);
        instructionText[i] << line.c_str();
    }
    wxMutexLocker predictorLock(predictorAccess);
    predictor.adjustPredictor(
            dataCollector.getAllValuesSize(), instructions);
    gestures = instructions;
    if (LearningMode::EMBEDDED_SIGNAL == learningMode &&
        gestureFiles < instructions)
    gestures = gestureFiles;

    return true;
}

bool APControl::saveInstructions(const wxString& filename)
{
    std::ofstream fileStream(filename.c_str(), std::ofstream::out);
    if (!fileStream.is_open()) return false;

    fileStream <<  "Instructions:" << std::endl;
    wxMutexLocker instructionLock(instructionAccess);
    for (unsigned int i = 0; i < instructions; ++i)
    {
        fileStream << instructionText[i] << std::endl;
    }
    return true;
}

ma::Str APControl::getCurrentInstruction()
{
    wxMutexLocker instructionLock(instructionAccess);
    return currentInstruction;
}

void APControl::setCurrentInstruction(const ma::Str& instruction)
{
    wxMutexLocker instructionLock(instructionAccess);
    currentInstruction = instruction;
}

unsigned int APControl::getInstructions() const
{
    return instructions;
}

ma::Str APControl::getInstruction(const unsigned int instructionIndex)
{
    if (instructionIndex >= instructions)
        return ma::Str("Unknown Instruction");
    wxMutexLocker instructionLock(instructionAccess);
    return instructionText[instructionIndex];
}

void APControl::setInstructions(const ma::Str& newInstructions)
{
    wxMutexLocker instructionLock(instructionAccess);

    // Count instructions.
    ma::StrStream countStream(newInstructions);
    ma::Str line;
    instructions = 0;
    while(ma::getNext(countStream, line))
    {
        // Ignore blank lines.
        if (line.length() != 0) ++instructions;
    }

    if (instructionText != 0) delete[] instructionText;
    instructionText = new ma::Str[instructions];

    // Populate instructions array.
    ma::StrStream populateStream(newInstructions);
    unsigned int lineIndex = 0;
    while(ma::getNext(populateStream, line))
    {
        // Ignore blank lines.
        if (line.length() != 0) instructionText[lineIndex++] = line;
    }

    wxMutexLocker predictorLock(predictorAccess);
    predictor.adjustPredictor(
            dataCollector.getAllValuesSize(), instructions);

    gestures = instructions;
    if (LearningMode::EMBEDDED_SIGNAL == learningMode &&
        gestureFiles < instructions)
    gestures = gestureFiles;
}

unsigned int APControl::getLearningInstancesRemaining() const
{
    if (isPlaybackPlaying()) return playback->getLoopPlaybackCountRemaining();
    return learningInstancesRemaining;
}

unsigned int APControl::getLearningInstancesPerInstruction() const
{
    return learningInstancesPerInstruction;
}

void APControl::setLearningInstancesPerInstruction(
        const unsigned int learningInstancesPerInstruction)
{
    this->learningInstancesPerInstruction = learningInstancesPerInstruction;
}

unsigned int APControl::getLearningTranches() const
{
    return learningTranches;
}

void APControl::setLearningTranches(const unsigned int learningTranches)
{
    this->learningTranches = learningTranches;
}

unsigned int APControl::getLearningShowTime() const
{
    return showTime;
}

void APControl::setLearningShowTime(const unsigned int showTime)
{
    this->showTime = showTime;
}


wxString APControl::getRecordFilename()
{
    return wxString(logger->getLogFilename().c_str());
}

void APControl::setRecordFilename(const wxString& filename)
{
    logger->setLogFilename(ma::Str(filename.c_str()));
}

void APControl::startRecording()
{
    setStatusMessage("Recording.");
    logger->startLogging();

    if (isLoopPaused() ||
        learningState <= LearningState::COUNT_DOWN ||
        (!useNoSignal && !signal) ||
        learningState == LearningState::DONE)
    {
        logger->pauseLogging();
    }
}

void APControl::stopRecording()
{
    setStatusMessage("Recording stopped.");
    logger->stopLogging();
}

bool APControl::isRecording()
{
    return logger->isLogging();
}

void APControl::displayPackets(bool display)
{
    dataCollector.displayPackets(display);
}

void APControl::setRealtime(bool realTime)
{
    playback->setRealtime(realTime);
}

wxString APControl::getPlaybackFilename()
{
    return wxString(playback->getLogFilename().c_str());
}

void APControl::setPlaybackFilename(const wxString& filename)
{
    playback->setLogFilename(ma::Str(filename.c_str()));
}

void APControl::startPlayback()
{
    stopTimer(SAMPLE_TIMER);
    playback->startPlayback();
}

void APControl::pausePlayback()
{
    playback->pausePlayback();
}

void APControl::stopPlayback()
{
    playback->stopPlayback();
    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
    case LearningMode::EMBEDDED_SIGNAL:
        startTimer(SAMPLE_TIMER, getTimerPeriod(SAMPLE_TIMER));
        break;
    case LearningMode::EMBEDDED_SNAP:
        break;
    }
}

bool APControl::isPlaybackPlaying() const
{
    return playback->isPlaying();
}

bool APControl::isPlaybackPaused() const
{
    return playback->isPaused();
}

bool APControl::isPlaybackLooping() const
{
    return playback->isPlaybackLooping();
}

void APControl::loopPlayback(const bool enable)
{
    playback->loopPlayback(enable);
}

int APControl::getLoopPlaybackCount() const
{
    return playback->getLoopPlaybackCount();
}

void APControl::setLoopPlaybackCount(const int loopCount)
{
    playback->setLoopPlaybackCount(loopCount);
}

void APControl::startPredictorLearning()
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.sequenceReset();
    predictor.setLearning(true);
}

void APControl::stopPredictorLearning()
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.setLearning(false);
}

void APControl::tellPredictorToSampleData()
{
    sampleData();
}

void APControl::tellPredictorSequenceReset()
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.sequenceReset();
}

void APControl::tellPredictorActualResult(unsigned int instructionIndex)
{
    wxMutexLocker predictorLock(predictorAccess);
    predictor.actualResult(instructionIndex);
    ++predictorSamples;
}

wxString APControl::getPredictionLogFilename() const
{
    return wxString(predictionLogFilename.c_str());
}

void APControl::setPredictionLogFilename(const wxString& filename)
{
    predictionLogFilename = ma::Str(filename.c_str());
}

void APControl::recordLearnerSettings()
{
    if (!learnerSettingsChanged) return;
    predictionLogFile.open(predictionLogFilename.c_str(),
            std::fstream::out | std::fstream::app);
    if (predictionLogFile.is_open())
    {
        predictionLogFile << std::endl;
        switch (predictor.getLearnerType())
        {
        case ma::LearnerType::MCMCLearner:
            predictionLogFile << "Type: MCMC Learner";
            break;
        case ma::LearnerType::MCNetLearner:
            predictionLogFile << "Type: MC Net Learner";
            break;
        case ma::LearnerType::TDNetLearner:
            predictionLogFile << "Type: TD Net Learner";
            break;
        }
        predictionLogFile << std::endl << "Learning rate: ";
        predictionLogFile << predictor.getLearningRate() << std::endl;
        predictionLogFile << "Discount rate: ";
        predictionLogFile << predictor.getDiscountRate() << std::endl;
        if (isNetLearner())
        {
            predictionLogFile << "Regularisation Factor: ";
            predictionLogFile << predictor.getRegularisationFactor() << std::endl;
        }
        if (isTDNetLearner())
        {
            predictionLogFile << "Prediction Discount Rate: ";
            predictionLogFile << predictor.getLambda() << std::endl;
        }
        predictionLogFile << "Sample rate: ";
        predictionLogFile << getSamplePeriod() << std::endl;
        predictionLogFile.close();
    }
    learnerSettingsChanged = false;
}

void APControl::recordIMUSettings(bool changed)
{
    recordLearnerSettings();
    if (!changed && predictorTranches != 0) return;
    predictionLogFile.open(predictionLogFilename.c_str(),
            std::fstream::out | std::fstream::app);
    if (predictionLogFile.is_open())
    {
        // Record current time.
        std::time_t timeNow = std::time(0);
        struct std::tm* now = std::localtime(&timeNow);
        predictionLogFile << std::endl << now->tm_year + 1900 << "-";
        predictionLogFile << std::setfill('0') << std::setw(2) << now->tm_mon;
        predictionLogFile << "-";
        predictionLogFile << std::setfill('0') << std::setw(2) << now->tm_mday;
        predictionLogFile << " ";
        predictionLogFile << std::setfill('0') << std::setw(2) << now->tm_hour;
        predictionLogFile << ":";
        predictionLogFile << std::setfill('0') << std::setw(2) << now->tm_min;
        predictionLogFile << std::endl;

        // Record imu settings.
        predictionLogFile << "IMUs: " << dataCollector.getIMUs() << ", ";
        predictionLogFile << "Using: ";
        bool any = false;
        if (dataCollector.isUsingQuaternions())
        {
            if (any) predictionLogFile << ", " << std::endl;
            predictionLogFile << "Quaternions(";
            predictionLogFile << dataCollector.getQuaternionHistoryLength();
            predictionLogFile << ")";
            any = true;
        }
        if (dataCollector.isUsingAccelerometers())
        {
            if (any) predictionLogFile << ", " << std::endl;
            predictionLogFile << "Accelerometers(";
            predictionLogFile << dataCollector.getAccelerometerHistoryLength();
            predictionLogFile << "): ";
            predictionLogFile << dataCollector.getMasterAccelerometerFilter()->
                    getDescription();
            any = true;
        }
        if (dataCollector.isUsingGyroscopes())
        {
            if (any) predictionLogFile << ", " << std::endl;
            predictionLogFile << "Gyroscopes(";
            predictionLogFile << dataCollector.getGyroscopeHistoryLength();
            predictionLogFile << "): ";
            predictionLogFile << dataCollector.getMasterGyroscopeFilter()->
                    getDescription();
            any = true;
        }
        if (dataCollector.isUsingMagnetometers())
        {
            if (any) predictionLogFile << ", " << std::endl;
            predictionLogFile << "Magnetometers(";
            predictionLogFile << dataCollector.getMagnetometerHistoryLength();
            predictionLogFile << "): ";
            predictionLogFile << dataCollector.getMasterMagnetometerFilter()->
                    getDescription();
            any = true;
        }
        if (dataCollector.isUsingAnalogue())
        {
            if (any) predictionLogFile << ", " << std::endl;
            predictionLogFile << "Analogue(";
            predictionLogFile << dataCollector.getAnalogueHistoryLength();
            predictionLogFile << "): ";
            predictionLogFile << dataCollector.getMasterAnalogueFilter()->
                    getDescription();
            any = true;
        }
        if (!any) predictionLogFile << "Nothing";
        predictionLogFile << std::endl;
        predictionLogFile.close();
    }
}

void APControl::recordPredictionMetaData()
{
    predictionLogFile.open(predictionLogFilename.c_str(),
            std::fstream::out | std::fstream::app);
    if (predictionLogFile.is_open())
    {
        // Record learning instances.
        predictionLogFile << "Tranche(Total Samples): ";
        predictionLogFile << (++predictorTranches) << "(";
        predictionLogFile << predictorSamples << ")" << std::endl;
        predictionLogFile.close();
    }
}

void APControl::recordPrediction()
{
    predictionLogFile.open(predictionLogFilename.c_str(),
            std::fstream::out | std::fstream::app);
    if (predictionLogFile.is_open())
    {
        wxMutexLocker instructionLock(instructionAccess);
        predictionLogFile << currentInstruction.c_str() << ":\t";
        predictionLogFile << getPrediction() << std::endl;
        predictionLogFile.close();
    }
}

void APControl::addBlankLineToPredicitionLogFile()
{
    predictionLogFile.open(predictionLogFilename.c_str(),
            std::fstream::out | std::fstream::app);
    if (predictionLogFile.is_open())
    {
        predictionLogFile << std::endl;
        predictionLogFile.close();
    }
}

void APControl::controlMouse(bool controllingMouse)
{
    this->controllingMouse = controllingMouse;
    mouse.controlMouse(controllingMouse);
}

unsigned int APControl::getMouseSensitivity() const
{
    return mouse.getSensitivity();
}

void APControl::setMouseSensitivity(const unsigned int milliSecondDelay)
{
    mouse.setSensitivity(milliSecondDelay);
}

void APControl::timerFunction(const unsigned int id)
{
    switch (id)
    {
    case SAMPLE_TIMER:
        sampleData(); break;
    }
}

void APControl::loopFunction()
{
    switch (learningState)
    {
    case LearningState::INITIALISE_LEARNING:
        initialiseLearning();
        break;
    case LearningState::INITIALISE_LEARNING_TRANCHE:
        initialiseLearningTranche();
        break;
    case LearningState::INITIALISE_COUNTDOWN:
        initialiseCountDown();
        break;
    case LearningState::COUNT_DOWN:
        displayCountDown();
        break;
    case LearningState::INITIALISE_LEARNING_STEP:
        initialiseLearningStep();
        break;
    case LearningState::LEARNING:
        displayLearning();
        break;
    case LearningState::INITIALISE_TESTING:
        initialiseTesting();
        break;
    case LearningState::INITIALISE_TESTING_STEP:
        initialiseTestingStep();
        break;
    case LearningState::TESTING:
        displayTesting();
        break;
    case LearningState::DONE:
        return;
    }
}

void APControl::initialiseLearning()
{
    currentLearningTranche = 0;
    triggerPoint = (averageSampleLength + dataCollector.getHistoryLength()) / 2;
    learningState = LearningState::INITIALISE_LEARNING_TRANCHE;
    recordIMUSettings();
    logger->unPauseLogging();
    logger->recordIMUSettings();
    logger->pauseLogging();
}

void APControl::initialiseLearningTranche()
{
    logger->pauseLogging();

    if (currentLearningTranche >= learningTranches)
    {
        setStatusMessage(ma::Str());
        currentInstruction = ma::Str("Done");
        learningState = LearningState::DONE;
        clearFileSerial();
        return;
    }

    ma::Str message("Starting tranche ");
    message += ma::toStr(currentLearningTranche + 1);
    message += ma::Str(" of ") + ma::toStr(learningTranches);
    setStatusMessage(message);

    if (learningInstructionUsed) delete[] learningInstructionUsed;
    learningInstructionUsed = new unsigned int[gestures]();
    learningInstancesRemaining =
            learningInstancesPerInstruction * gestures;

    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
        learningState = LearningState::INITIALISE_COUNTDOWN;
        break;
    case LearningMode::EMBEDDED_SIGNAL:
    case LearningMode::EMBEDDED_SNAP:
        initialiseEmbeddedTranche();
        learningState = LearningState::INITIALISE_LEARNING_STEP;
        break;
    }
}

void APControl::initialiseEmbeddedTranche()
{
    if (gestures == 0)
    {
        std::cout << "No gesture files found!" << std::endl;
        return;
    }
    clearFileSerial();
    fileSerial = new ma::FileSerial*[gestures]();
    fileSerialUsed = gestures;
    for (unsigned int gesture = 0; gesture < gestures; ++gesture)
    {
        ma::Str filename = recordedDataDirectory +
                "G" + ma::toStr(gesture + 1) +
                "T" + ma::toStr(currentLearningTranche + 1) + ".csv";
        fileSerial[gesture] = new ma::FileSerial();
        if (!fileSerial[gesture]->setPort(filename, std::ios::in))
            std::cout << "Failed to open " << filename << std::endl;
    }
    initialFileSerial = dataCollector.setFileSerial(fileSerial[0], 0);

    signal = false;
    wxMutexLocker instructionLock(instructionAccess);
    currentInstruction = ma::Str("No Signal");

    logger->unPauseLogging();
    startPredictorLearning();
    logger->startLearning();

    if (useNoSignal)
    {
        logger->currentInstruction(currentInstruction.c_str());
    }
    else
    {
        stopPredictorLearning();
        logger->pauseLogging();
    }
}

void APControl::initialiseCountDown()
{
    countDown = COUNTDOWN_SECONDS;

    if (countDown >  0)
    {
        learningState = LearningState::COUNT_DOWN;
    }
    else
    {
        logger->unPauseLogging();
        startPredictorLearning();
        logger->startLearning();
        learningState = LearningState::INITIALISE_LEARNING_STEP;
    }
}

void APControl::displayCountDown()
{
    {
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::toStr(countDown);
    }
    wxMilliSleep(1000);
    if (!isLoopPaused() && learningState != LearningState::DONE)
    {
        if (--countDown == 0)
        {
            logger->unPauseLogging();
            startPredictorLearning();
            logger->startLearning();
            learningState = LearningState::INITIALISE_LEARNING_STEP;
        }
    }
}

void APControl::initialiseLearningStep()
{
    if (learningInstancesRemaining == 0)
    {
        learningState = LearningState::INITIALISE_TESTING;
        return;
    }

    instructionIndex = getNextInstructionIndex();

    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
        initialiseInteractiveStep();
        break;
    case LearningMode::EMBEDDED_SIGNAL:
    case LearningMode::EMBEDDED_SNAP:
        initialiseEmbeddedStep();
        break;
    }

    learningState = LearningState::LEARNING;
}

void APControl::initialiseInteractiveStep()
{
    wxMutexLocker instructionLock(instructionAccess);
    currentInstruction = instructionText[instructionIndex];
    logger->currentInstruction(currentInstruction.c_str());

    showTimeRemaining = showTime;
}

void APControl::initialiseEmbeddedStep()
{
    if (!dataCollector.setFileSerial(fileSerial[instructionIndex], 0))
        std::cout << "Failed to set FileSerial" << std::endl;
}

void APControl::displayLearning()
{
    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
        displayInteractiveLearning();
        break;
    case LearningMode::EMBEDDED_SIGNAL:
        displayEmbeddedLearning();
        break;
    case LearningMode::EMBEDDED_SNAP:
        displayEmbeddedSnapLearning();
        break;
    }
}

void APControl::displayInteractiveLearning()
{
    wxMilliSleep(1000);
    if (!isLoopPaused() && learningState != LearningState::DONE)
    {
        if (--showTimeRemaining == 0)
        {
            tellPredictorActualResult(instructionIndex);
            logger->actualResult(instructionIndex);
            --learningInstancesRemaining;
            learningState = LearningState::INITIALISE_LEARNING_STEP;
        }
    }
}

void APControl::displayEmbeddedLearning()
{
    unsigned int newSignal = dataCollector.getCurrentSignal(0);

    // Signal switched on.
    if (newSignal == 255 && signal == false)
    {
        if (useNoSignal)
        {
            tellPredictorActualResult(0);
            logger->actualResult(0);
        }
        else
        {
            startPredictorLearning();
            logger->unPauseLogging();
        }

        signal = true;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = instructionText[instructionIndex];
        logger->currentInstruction(currentInstruction.c_str());
    }

    // Signal switched off.
    if (newSignal == 0 && signal == true)
    {
        tellPredictorActualResult(instructionIndex);
        logger->actualResult(instructionIndex);
        --learningInstancesRemaining;

        signal = false;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::Str("No Signal");

        if (useNoSignal)
        {
            logger->currentInstruction(currentInstruction.c_str());
        }
        else
        {
            stopPredictorLearning();
            logger->pauseLogging();
        }

        learningState = LearningState::INITIALISE_LEARNING_STEP;
    }
}

void APControl::displayEmbeddedSnapLearning()
{
    unsigned int newSignal = dataCollector.getCurrentSignal(0);

    // Signal switched on.
    if (newSignal == 255 && signal == false)
    {
        if (useNoSignal)
        {
            sampleData();
            tellPredictorActualResult(0);
            logger->actualResult(0);
        }
        else
        {
            startPredictorLearning();
            logger->unPauseLogging();
        }

        signal = true;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = instructionText[instructionIndex];
        logger->currentInstruction(currentInstruction.c_str());
    }

    // Signal switched off.
    if (newSignal == 0 && signal == true)
    {
        sampleData();
        tellPredictorActualResult(instructionIndex);
        logger->actualResult(instructionIndex);
        --learningInstancesRemaining;

        signal = false;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::Str("No Signal");

        if (useNoSignal)
        {
            logger->currentInstruction(currentInstruction.c_str());
        }
        else
        {
            stopPredictorLearning();
            logger->pauseLogging();
        }

        learningState = LearningState::INITIALISE_LEARNING_STEP;
    }
}

void APControl::initialiseTesting()
{
    setStatusMessage("Recording predictions.");

    predictionIndex = 0;
    stopPredictorLearning();
    addBlankLineToPredicitionLogFile();
    recordPredictionMetaData();

    bool wasPaused = logger->isPaused();
    logger->unPauseLogging();
    logger->stopLearning();
    logger->addingBlankLineToPredicitionLogFile();
    logger->recordingPredictionMetaData();
    if (wasPaused) logger->pauseLogging();

    learningState = LearningState::INITIALISE_TESTING_STEP;
}

void APControl::initialiseTestingStep()
{
    if (predictionIndex >= gestures)
    {
        ++currentLearningTranche;
        learningState = LearningState::INITIALISE_LEARNING_TRANCHE;
        return;
    }

    instructionIndex = predictionIndex;

    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
        initialiseInteractiveStep();
        break;
    case LearningMode::EMBEDDED_SIGNAL:
    case LearningMode::EMBEDDED_SNAP:
        initialiseEmbeddedStep();
        break;
    }

    learningState = LearningState::TESTING;
}

void APControl::displayTesting()
{
    switch (learningMode)
    {
    case LearningMode::INTERACTIVE:
        displayInteractiveTesting();
        break;
    case LearningMode::EMBEDDED_SIGNAL:
        displayEmbeddedTesting();
        break;
    case LearningMode::EMBEDDED_SNAP:
        displayEmbeddedSnapTesting();
        break;
    }
}

void APControl::displayInteractiveTesting()
{
    wxMilliSleep(1000);
    if (!isLoopPaused() && learningState != LearningState::DONE)
    {
        --showTimeRemaining;
        if (showTimeRemaining == 1)
        {
            recordPrediction();
            logger->recordingPrediction();
        }
        else if (showTimeRemaining == 0)
        {
            ++predictionIndex;
            learningState = LearningState::INITIALISE_TESTING_STEP;
        }
    }
}

void APControl::displayEmbeddedTesting()
{
    unsigned int newSignal = dataCollector.getCurrentSignal(0);

    // Signal switched on.
    if (newSignal == 255 && signal == false)
    {
        logger->unPauseLogging();

        signal = true;
        sampleStarted = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        predictionRecorded = false;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = instructionText[instructionIndex];
        logger->currentInstruction(currentInstruction.c_str());
    }

    // Signal switched off.
    if (newSignal == 0 && signal == true)
    {
        ++predictionIndex;
        logger->pauseLogging();

        signal = false;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::Str("No Signal");

        learningState = LearningState::INITIALISE_TESTING_STEP;
    }

    if (signal && !predictionRecorded)
    {
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        if ((now - sampleStarted).count() >= triggerPoint)
        {
            recordPrediction();
            logger->recordingPrediction();
            predictionRecorded = true;
        }
    }
}

void APControl::displayEmbeddedSnapTesting()
{
    unsigned int newSignal = dataCollector.getCurrentSignal(0);

    // Signal switched on.
    if (newSignal == 255 && signal == false)
    {
        logger->unPauseLogging();

        signal = true;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = instructionText[instructionIndex];
        logger->currentInstruction(currentInstruction.c_str());
    }

    // Signal switched off.
    if (newSignal == 0 && signal == true)
    {
        recordPrediction();
        logger->recordingPrediction();

        ++predictionIndex;
        logger->pauseLogging();

        signal = false;
        wxMutexLocker instructionLock(instructionAccess);
        currentInstruction = ma::Str("No Signal");

        learningState = LearningState::INITIALISE_TESTING_STEP;
    }
}

unsigned int APControl::getNextInstructionIndex()
{
    unsigned int previousIndex = instructionIndex;
    unsigned int instructionIndex = -1;
    do
    {
        instructionIndex = rand() % gestures;
    }
    while (learningInstructionUsed[instructionIndex] >=
                learningInstancesPerInstruction ||
            (learningInstancesRemaining > learningInstancesPerInstruction
                && instructionIndex == previousIndex));
    ++learningInstructionUsed[instructionIndex];

    return instructionIndex;
}

void APControl::sampleData()
{
    wxMutexLocker dataCollectorLock(dataCollectorAccess);
    wxMutexLocker predictorLock(predictorAccess);
    predictor.nextState(dataCollector.getAllValues());
    logger->sampleData();
}

void APControl::setInteractiveLearningValues()
{
    gestures = instructions;
    learningTranches = DEFAULT_LEARNING_TRANCHES;
    learningInstancesPerInstruction = DEFAULT_LEARNING_INSTANCES_PER_INSTRUCTION;
}

void APControl::setLearningValuesFromGestureTrancheFilenames()
{
    wxString progressMessage(wxT(
            "Calculate learning values from gesture-tranche files."));
    wxProgressDialog progress(wxT("Calculating Learning Values"), progressMessage);
    std::ifstream recordedDataFile;

    // Count gestures.
    gestureFiles = 0;
    bool gestureFound = true;
    while (gestureFound)
    {
        ma::Str filename = recordedDataDirectory +
                "G" + ma::toStr(++gestureFiles) + "T1.csv";
        recordedDataFile.open(filename.c_str(), std::ios::in);
        if (recordedDataFile.is_open())
            recordedDataFile.close();
        else
        {
            gestureFound = false;
            --gestureFiles;
        }
    }
    progressMessage = "Found ";
    progressMessage << gestureFiles << " gesture file";
    if (gestureFiles != 1) progressMessage << "s";
    progressMessage << ".";
    std::cout << progressMessage << std::endl;
    progress.Update(0, progressMessage);

    // Count tranches.
    unsigned int tranches[gestureFiles] = {0};
    for (unsigned int gesture = 0; gesture < gestureFiles; ++gesture)
    {
        bool trancheFound = true;
        while (trancheFound)
        {
            ma::Str filename = recordedDataDirectory +
                    "G" + ma::toStr(gesture+1) +
                    "T" + ma::toStr(++tranches[gesture]) + ".csv";
            recordedDataFile.open(filename.c_str(), std::ios::in);
            if (recordedDataFile.is_open())
                recordedDataFile.close();
            else
            {
                trancheFound = false;
                --tranches[gesture];
            }
        }
        progressMessage = "Gesture ";
        progressMessage << gesture << " has ";
        progressMessage << tranches[gesture] << " tranche";
        if (tranches[gesture] != 1) progressMessage << "s";
        progressMessage << ".";
        progress.Update(10 * (gesture + 1) / gestureFiles, progressMessage);
    }

    // Count samples and sample lengths.
    ma::FileSerial fileSerial;
    unsigned int** samples = new unsigned int*[gestureFiles];
    unsigned int** samplesLength = new unsigned int*[gestureFiles];
    unsigned char packet[256];
    for (unsigned int gesture = 0; gesture < gestureFiles; ++gesture)
    {
        samples[gesture] = new unsigned int[tranches[gesture]]();
        samplesLength[gesture] = new unsigned int[tranches[gesture]]();
        for (unsigned int tranche = 0; tranche < tranches[gesture]; ++tranche)
        {
            ma::Str filename =
                    "G" + ma::toStr(gesture + 1) +
                    "T" + ma::toStr(tranche + 1) + ".csv";
            if (!fileSerial.setPort(recordedDataDirectory + filename))
            {
                std::cout << "Failed to open " << filename << std::endl;
                continue;
            }
            std::cout << "Checking " << filename << "..." << std::endl;
            bool wasSignal = false;
            while (fileSerial.isConnected())
            {
                unsigned int packetLength = 256;
                packetLength = fileSerial.readData(packet, packetLength);
                if (packetLength == 36)
                {
                    if (!wasSignal && packet[35] == 255)
                    {
                        ++samples[gesture][tranche];
                        wasSignal = true;
                    }
                    else if (wasSignal && packet[35] == 0)
                    {
                        wasSignal = false;
                    }
                    if (packet[35] == 255)
                    {
                        ++samplesLength[gesture][tranche];
                    }
                }
            }
            progressMessage = filename.c_str();
            progressMessage << " has " << samples[gesture][tranche] << " sample";
            if (samples[gesture][tranche] != 1) progressMessage << "s";
            progressMessage << " of average length ";
            progressMessage << samplesLength[gesture][tranche] /
                    samples[gesture][tranche] << ".";
            std::cout << progressMessage << std::endl;
            progress.Update(10 + 90 * gesture / gestureFiles +
                    90 * tranche / gestureFiles / tranches[gesture],
                    progressMessage);
        }
    }

    // Determine minimum number of samples per file.
    unsigned int minimumSamples = 0;
    if (gestureFiles > 0 && tranches[0] > 0) minimumSamples = samples[0][0];
    for (unsigned int gesture = 0; gesture < gestureFiles; ++gesture)
    {
        std::cout << "Gesture " << gesture + 1 << ":\t";
        if (tranches[gesture] > 0)
        {
            std::cout << samples[gesture][0];
            std::cout << "(" << samplesLength[gesture][0] /
                    samples[gesture][0] << ")";
            if (samples[gesture][0] < minimumSamples)
                    minimumSamples = samples[gesture][0];
        }
        for (unsigned int tranche = 1; tranche < tranches[gesture]; ++tranche)
        {
            std::cout << ",\t" << samples[gesture][tranche];
            std::cout << "(" << samplesLength[gesture][tranche] /
                    samples[gesture][tranche] << ")";
            if (samples[gesture][tranche] < minimumSamples)
                    minimumSamples = samples[gesture][tranche];
        }
        std::cout << std::endl;
    }
    if (minimumSamples > 2)
    {
        std::cout << "Minimum samples = " << minimumSamples << std::endl;
        learningInstancesPerInstruction = minimumSamples - 1;
    }
    else
    {
        learningInstancesPerInstruction = 0;
    }

    // Determine average sample length.
    averageSampleLength = 0;
    unsigned int totalSamples = 0;
    for (unsigned int gesture = 0; gesture < gestureFiles; ++gesture)
    {
        for (unsigned int tranche = 0; tranche < tranches[gesture]; ++tranche)
        {
            averageSampleLength += samplesLength[gesture][tranche];
            totalSamples += samples[gesture][tranche];
        }
    }
    if (totalSamples > 0)
    {
        std::cout << "Average sample length: ";
        std::cout << (averageSampleLength /= totalSamples) << std::endl;
    }

    // Set learning tranches to minimum number of tranches.
    learningTranches = 0;
    if (gestureFiles > 0)
    {
        learningTranches = tranches[0];
        for (unsigned int gesture = 1; gesture < gestureFiles; ++gesture)
        {
            if (tranches[gesture] < learningTranches)
            {
                std::cout << "Gesture " << gesture << " only has ";
                std::cout << tranches[gesture] << " tranches!" << std::endl;
                learningTranches = tranches[gesture];
            }
        }
    }

    // Display result.
    ma::StrStream message("Directory contains: ");
    message << gestureFiles << " Gesture";
    if (gestureFiles != 1) message << "s";
    if (gestureFiles > 0)
    {
        message << " with " << learningTranches << " Tranche";
        if (learningTranches != 1) message << "s";
    }
    message << " containing " << minimumSamples << " sample";
    if (minimumSamples != 1) message << "s";
    message << " at " << averageSampleLength << "Hz.";
    setStatusMessage(message.toStr());

    // Clean up.
    for (unsigned int gesture = 0; gesture < gestureFiles; ++gesture)
    {
        delete[] samplesLength[gesture];
        delete[] samples[gesture];
    }
    delete[] samplesLength;
    delete[] samples;

    gestures = gestureFiles;
    if (instructions < gestureFiles)
        gestures = instructions;
}

void APControl::clearFileSerial()
{
    if (initialFileSerial)
    {
        dataCollector.resetFileSerialTimer(0);
        dataCollector.setFileSerial(initialFileSerial, 0);
        initialFileSerial = 0;
    }
    if (fileSerial)
    {
        for (unsigned int index = 0; index < fileSerialUsed; ++index)
        {
            if (fileSerial[index]) delete fileSerial[index];
            fileSerial[index] = 0;
        }
        delete[] fileSerial;
        fileSerial = 0;
        fileSerialUsed = 0;
    }
}
