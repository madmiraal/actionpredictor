// (c) 2016: Marcel Admiraal

#include "apinstructionpanel.h"

#include <wx/dcbuffer.h>
#include <wx/sizer.h>

APInstructionPanel::APInstructionPanel(wxWindow* parent, APControl* control) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxFULL_REPAINT_ON_RESIZE),
        control(control)
{
    wxBoxSizer* panelSizer = new wxBoxSizer(wxVERTICAL);
    instructionPanel = new ma::FitTextPanel(this);
    panelSizer->Add(instructionPanel, 9, wxEXPAND);
    remainingPanel = new ma::FitTextPanel(this, wxEmptyString, *wxBLACK,
            *wxLIGHT_GREY);
    panelSizer->Add(remainingPanel, 1, wxEXPAND);
    SetSizerAndFit(panelSizer);

    Bind(wxEVT_ERASE_BACKGROUND, &APInstructionPanel::stopErase, this);
    Bind(wxEVT_IDLE, &APInstructionPanel::onIdle, this);
}

APInstructionPanel::~APInstructionPanel()
{
}

void APInstructionPanel::stopErase(wxEraseEvent& event)
{
    // Intercepting erase event to stop erasing.
}

void APInstructionPanel::onIdle(wxIdleEvent& evt)
{
    instructionPanel->setText(control->getCurrentInstruction().c_str());
    wxString remaining(wxT("Remaining: "));
    remaining << control->getLearningInstancesRemaining();
    remainingPanel->setText(remaining);
    Refresh();
}
