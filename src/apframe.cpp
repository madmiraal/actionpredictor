// (c) 2016 - 2019: Marcel Admiraal

#include "apframe.h"

#include <wx/menu.h>
#include <wx/sizer.h>
#include <wx/choicdlg.h>
#include <wx/dirdlg.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/numdlg.h>
#include <wx/textdlg.h>

#include "apinstructionpanel.h"
#include "appredictionpanel.h"

#include "icons.h"
#include "imuconfig.h"
#include "fittextpanel.h"
#include "texteditordialog.h"

// Control IDs.
enum
{
    ID_SET_RECORDED_DATA_DIRECTORY,
    ID_INTERACTIVE,
    ID_EMBEDDED_SIGNAL,
    ID_EMBEDDED_SNAP,
    ID_SET_LEARN_RATE,
    ID_SET_DISCOUNT_RATE,
    ID_SET_REGULARISATIION_FACTOR,
    ID_SET_LAMBDA,
    ID_SET_INPUT_RATE,
    ID_SET_LEARN_INSTANCES,
    ID_SET_LEARN_TRANCHES,
    ID_SET_SHOW_TIME,
    ID_SET_DISPLAY_PACKETS,
    ID_LOAD_INSTRUCTIONS,
    ID_SAVE_INSTRUCTIONS,
    ID_EDIT_INSTRUCTIONS,
    ID_SET_PLAYBACK_FILE,
    ID_SET_REALTIME,
    ID_LOOP_PLAYBACK,
    ID_SET_RECORD_FILE,
    ID_SET_PREDICTION_FILE,
    ID_CONTROL_MOUSE,
    ID_SET_MOUSE_SENSITIVITY,
    ID_PAUSE_LEARN,
    ID_CONTINUE_LEARN,
    ID_LEARN,
    ID_PREDICT,
    ID_RECORD,
    ID_START_PLAYBACK,
    ID_PAUSE_PLAYBACK,
    ID_STOP
};

APFrame::APFrame() :
    ma::ControlFrame(new APControl(), 0, wxID_ANY, "Action Predictor",
            wxDefaultPosition, wxSize(506, 400)),
        pauseLearnTool(0), continueLearnTool(0),
        learnTool(0), predictTool(0), playbackStartTool(0), playbackPauseTool(0)
{
    apcontrol = (APControl*)control;

    SetIcon(wxIcon(wxT("predictor.ico")));

    // Create the menus.
    // File menu.
    wxMenu* fileMenu = new wxMenu();
    fileMenu->Append(wxID_EXIT, wxT("&Quit"));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::quit, this, wxID_EXIT);
    fileMenu->Append(ID_SET_RECORDED_DATA_DIRECTORY,
            wxT("Recorded Data Location"),
            wxT("View and set the recorded data location."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setRecordedDataDirectory, this,
            ID_SET_RECORDED_DATA_DIRECTORY);
    fileMenu->AppendRadioItem(ID_INTERACTIVE, wxT("Interactive"),
            wxT("Learn by interacting in real-time."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLearningMode, this,
            ID_INTERACTIVE);
    fileMenu->AppendRadioItem(ID_EMBEDDED_SIGNAL, wxT("Embedded Signal"),
            wxT("Learn by using recorded data with embedded signal."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLearningMode, this,
            ID_EMBEDDED_SIGNAL);
    fileMenu->AppendRadioItem(ID_EMBEDDED_SNAP, wxT("Embedded Snapshot"),
            wxT("Learn by using recorded data and snapshots."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLearningMode, this,
            ID_EMBEDDED_SNAP);
    switch (apcontrol->getLearningMode())
    {
    case LearningMode::INTERACTIVE:
        fileMenu->Check(ID_INTERACTIVE, true);
        break;
    case LearningMode::EMBEDDED_SIGNAL:
        fileMenu->Check(ID_EMBEDDED_SIGNAL, true);
        break;
    case LearningMode::EMBEDDED_SNAP:
        fileMenu->Check(ID_EMBEDDED_SNAP, true);
        break;
    }
    menuBar->Append(fileMenu, wxT("&File"));

    // Learning menu.
    wxMenu* learningMenu = new wxMenu();
    learningMenu->Append(wxID_FILE, wxT("New Learner"),
            wxT("Create a new learner for Predictor."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::newLearner, this, wxID_FILE);
    learningMenu->Append(wxID_OPEN, wxT("Load Learner"),
            wxT("Load a saved learner for Predictor."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::loadLearner, this, wxID_OPEN);
    learningMenu->Append(wxID_SAVE, wxT("Save Learner"),
            wxT("Save the learner for Predictor."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::saveLearner, this, wxID_SAVE);
    learningMenu->Append(ID_SET_LEARN_RATE, wxT("Set Learning Rate"),
            wxT("Set the learner's learning rate."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLearningRate, this,
            ID_SET_LEARN_RATE);
    learningMenu->Append(ID_SET_DISCOUNT_RATE, wxT("Set Discount Rate"),
            wxT("Set the learner's discount rate."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setDiscountRate, this,
            ID_SET_DISCOUNT_RATE);
    learningMenu->Append(ID_SET_REGULARISATIION_FACTOR,
            wxT("Set Regularisation Factor"),
            wxT("Set the net learner's regularisation factor."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setRegularisationFactor, this,
            ID_SET_REGULARISATIION_FACTOR);
    learningMenu->Append(ID_SET_LAMBDA,
            wxT("Set Prediction Discount"),
            wxT("Set the TD Net learner's prediction discount."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLambda, this,
            ID_SET_LAMBDA);
    learningMenu->Append(ID_SET_INPUT_RATE, wxT("Set Input Rate"),
            wxT("Set the learner's input sample rate."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setInputRate, this,
            ID_SET_INPUT_RATE);
    learningMenu->Append(ID_SET_LEARN_INSTANCES, wxT("Set Learning Instances"),
            wxT("Set the number of instances per instruction shown during learning."));
    Bind(wxEVT_COMMAND_MENU_SELECTED,
            &APFrame::setLearningInstancesPerInstruction, this,
            ID_SET_LEARN_INSTANCES);
    learningMenu->Append(ID_SET_LEARN_TRANCHES, wxT("Set Learning Tranches"),
            wxT("Set the number of tranches of instances during learning."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLearningTranches, this,
            ID_SET_LEARN_TRANCHES);
    learningMenu->Append(ID_SET_SHOW_TIME, wxT("Set Learning Show Time"),
            wxT("Set the time in seconds to show each learning instance."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setLearningShowTime, this,
            ID_SET_SHOW_TIME);
    menuBar->Append(learningMenu, wxT("Learning"));

    // Input menu.
    wxMenu* inputMenu = new wxMenu();
    inputMenu->AppendCheckItem(ID_SET_DISPLAY_PACKETS, wxT("Display Packets"),
            wxT("Set whether or not to display packets."));
    inputMenu->Check(ID_SET_DISPLAY_PACKETS, false);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setDisplayPackets, this,
            ID_SET_DISPLAY_PACKETS);
    inputMenu->Append(wxID_EXECUTE, wxT("Configure IMUs"),
            wxT("Configure the IMUs."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::configureIMUs, this,
            wxID_EXECUTE);
    inputMenu->Append(ID_LOAD_INSTRUCTIONS, wxT("Load Instructions"),
            wxT("Load the set of instructions from a file."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::loadInstructions, this,
            ID_LOAD_INSTRUCTIONS);
    inputMenu->Append(ID_SAVE_INSTRUCTIONS, wxT("Save Instructions"),
            wxT("Save the current instructions to a file."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::saveInstructions, this,
            ID_SAVE_INSTRUCTIONS);
    inputMenu->Append(ID_EDIT_INSTRUCTIONS, wxT("Edit Instructions"),
            wxT("Edit the current list of instructions."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::editInstructions, this,
            ID_EDIT_INSTRUCTIONS);
    inputMenu->Append(ID_SET_PLAYBACK_FILE, wxT("Set Playback File"),
            wxT("Set the file to use for playback."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setPlaybackFile, this,
            ID_SET_PLAYBACK_FILE);
    inputMenu->AppendCheckItem(ID_SET_REALTIME, wxT("Real-time Playback"),
            wxT("Set whether or not to playback in real-time."));
    inputMenu->Check(ID_SET_REALTIME, true);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setRealtime, this,
            ID_SET_REALTIME);
    inputMenu->AppendCheckItem(ID_LOOP_PLAYBACK, wxT("Loop Playback"),
            wxT("Set whether or not to loop the playback."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::loopPlayback, this,
            ID_LOOP_PLAYBACK);
    menuBar->Append(inputMenu, wxT("Inputs"));

    // Output menu.
    wxMenu* outputMenu = new wxMenu();
    outputMenu->Append(ID_SET_RECORD_FILE, wxT("Set Record File"),
            wxT("Set the file to use for recording."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setRecordFile, this,
            ID_SET_RECORD_FILE);
    outputMenu->Append(ID_SET_PREDICTION_FILE, wxT("Set Prediction Log File"),
            wxT("Set the file to use for logging predictions."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setPredictionLogFile, this,
            ID_SET_PREDICTION_FILE);
    outputMenu->AppendCheckItem(ID_CONTROL_MOUSE, wxT("Control &Mouse\tCTRL+M"),
            wxT("Output controls the mouse."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::controlMouse, this,
            ID_CONTROL_MOUSE);
    outputMenu->Append(ID_SET_MOUSE_SENSITIVITY, wxT("Set Mouse Sensitivity"),
            wxT("Set the millisecond delay between mouse movement predictions."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::setMouseSensitivity, this,
            ID_SET_MOUSE_SENSITIVITY);
    menuBar->Append(outputMenu, wxT("Outputs"));

    // Create the toolbar.
    wxImage::AddHandler(new wxPNGHandler);
    toolBar->AddTool(ID_PAUSE_LEARN, wxT("Pause"), pause1Icon,
            wxT("Pause Learning"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::pauseLearningToolClicked, this,
            ID_PAUSE_LEARN);

    toolBar->AddTool(ID_CONTINUE_LEARN, wxT("Continue"), goSignIcon,
            wxT("Continue Learning"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::pauseLearningToolClicked, this,
            ID_CONTINUE_LEARN);

    toolBar->AddTool(ID_LEARN, wxT("Learn"), learnIcon,
            wxT("Enable Learning"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::learnToolClicked, this,
            ID_LEARN);

    toolBar->AddTool(ID_PREDICT, wxT("Predict"), predictIcon,
            wxT("Predict Only"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::learnToolClicked, this,
            ID_PREDICT);

    toolBar->AddSeparator();

    toolBar->AddTool(ID_RECORD, wxT("Start Recording"), recordIcon,
            wxT("Start Recording"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::recordToolClicked, this,
            ID_RECORD);

    toolBar->AddTool(ID_START_PLAYBACK, wxT("Start Playback"), playIcon,
            wxT("Start Playback"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::startPlaybackClicked, this,
            ID_START_PLAYBACK);

    toolBar->AddTool(ID_PAUSE_PLAYBACK, wxT("Pause Playback"), pauseIcon,
            wxT("Pause Playback"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::pausePlaybackClicked, this,
            ID_PAUSE_PLAYBACK);

    toolBar->AddTool(ID_STOP, wxT("Stop Record/Playback"), stopIcon,
            wxT("Stop the Recording or Playback"));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &APFrame::stopToolClicked, this,
            ID_STOP);

    toolBar->Realize();

    // Remove and store unused toolbar buttons.
    if (apcontrol->isLearningPaused())
    {
        pauseLearnTool = toolBar->RemoveTool(ID_PAUSE_LEARN);
    }
    else
    {
        continueLearnTool = toolBar->RemoveTool(ID_CONTINUE_LEARN);
    }
    if (apcontrol->isLearning())
    {
        learnTool = toolBar->RemoveTool(ID_LEARN);
    }
    else
    {
        predictTool = toolBar->RemoveTool(ID_PREDICT);
    }
    if (apcontrol->isPlaybackPlaying() || apcontrol->isRecording())
    {
        toolBar->EnableTool(ID_RECORD, false);
    }
    else
    {
        toolBar->EnableTool(ID_STOP, false);
    }
    playbackPauseTool = toolBar->RemoveTool(ID_PAUSE_PLAYBACK);

    // Create panels.
    wxBoxSizer* frameSizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* instructionSizer = new wxBoxSizer(wxVERTICAL);
    wxPanel* instructionLabel = new ma::FitTextPanel(this, wxT("Instruction"),
            *wxBLACK, *wxLIGHT_GREY);
    wxPanel* instructionPanel = new APInstructionPanel(this, apcontrol);
    instructionSizer->Add(instructionLabel, 1, wxEXPAND);
    instructionSizer->Add(instructionPanel, 9, wxEXPAND);
    frameSizer->Add(instructionSizer, 1, wxEXPAND);

    wxBoxSizer* predictionSizer = new wxBoxSizer(wxVERTICAL);
    wxPanel* predictionLabel = new ma::FitTextPanel(this, wxT("Prediction"),
            *wxBLACK, *wxLIGHT_GREY);
    wxPanel* predictionPanel = new APPredictionPanel(this, apcontrol);
    predictionSizer->Add(predictionLabel, 1, wxEXPAND);
    predictionSizer->Add(predictionPanel, 9, wxEXPAND);
    frameSizer->Add(predictionSizer, 1, wxEXPAND);
    SetSizer(frameSizer);
}

APFrame::~APFrame()
{
    Unbind(wxEVT_COMMAND_MENU_SELECTED, &APFrame::configureIMUs, this);
    if (pauseLearnTool != 0) delete pauseLearnTool;
    if (continueLearnTool != 0) delete continueLearnTool;
    if (learnTool != 0) delete learnTool;
    if (predictTool != 0) delete predictTool;
    if (playbackStartTool != 0) delete playbackStartTool;
    if (playbackPauseTool != 0) delete playbackPauseTool;
}

void APFrame::setRecordedDataDirectory(wxCommandEvent& event)
{
    wxString directory(apcontrol->getRecordedDataDirectory());
    wxString windowTitle("Set Recorded Data Directory.");
    wxDirDialog setDirectoryDialog(this, windowTitle, directory);
    if (setDirectoryDialog.ShowModal() == wxID_CANCEL) return;
    apcontrol->setRecordedDataDirectory(setDirectoryDialog.GetPath());
}

void APFrame::setLearningMode(wxCommandEvent& event)
{
    switch (event.GetId())
    {
    case ID_INTERACTIVE:
        apcontrol->setLearningMode(LearningMode::INTERACTIVE);
        break;
    case ID_EMBEDDED_SIGNAL:
        apcontrol->setLearningMode(LearningMode::EMBEDDED_SIGNAL);
        break;
    case ID_EMBEDDED_SNAP:
        apcontrol->setLearningMode(LearningMode::EMBEDDED_SNAP);
        break;
    }
}

void APFrame::newLearner(wxCommandEvent& event)
{
    wxString choice[3];
    choice[0] = wxT("MCMC Learner");
    choice[1] = wxT("MC Net Learner");
    choice[2] = wxT("TD Net Learner");
    wxSingleChoiceDialog learnerDialog(this,
            wxT("Select the type of learner:"),
            wxT("Create New Learner for Predictor"), 3, choice);
    learnerDialog.SetSize(330, 220);
    if (learnerDialog.ShowModal() == wxID_CANCEL) return;
    ma::Vector layerSize;
    ma::NeuronType neuronType = ma::NeuronType::Logistic;
    switch (learnerDialog.GetSelection())
    {
    case 0:
        apcontrol->newMCMCLearner();
        break;
    case 1:
        getNetworkDetails(this, layerSize, neuronType);
        apcontrol->newMCNetLearner(layerSize, neuronType);
        break;
    case 2:
        getNetworkDetails(this, layerSize, neuronType);
        apcontrol->newTDNetLearner(layerSize, neuronType);
        break;
    default:
        break;
    }
}

void APFrame::loadLearner(wxCommandEvent& event)
{
    wxString filename("learner.dat");
    wxString windowTitle("Load Learner");
    bool success = false;
    while (!success)
    {
        wxFileDialog openFileDialog(this, windowTitle, wxEmptyString,
                filename, wxFileSelectorDefaultWildcardStr,
                wxFD_OPEN|wxFD_FILE_MUST_EXIST);
        if (openFileDialog.ShowModal() == wxID_CANCEL) return;
        success = apcontrol->loadLearner(openFileDialog.GetPath());
        if (!success)
        {
            wxMessageBox(wxT("Load Failed"));
        }
    }
}

void APFrame::saveLearner(wxCommandEvent& event)
{
    wxString filename("learner.dat");
    wxString windowTitle("Save Learner");
    bool success = false;
    while (!success)
    {
        wxFileDialog saveFileDialog(this, windowTitle, wxEmptyString,
                filename, wxFileSelectorDefaultWildcardStr,
                wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
        if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
        success = apcontrol->saveLearner(saveFileDialog.GetPath());
        if (!success)
        {
            wxMessageBox(wxT("Save Failed"));
        }
    }
}

void APFrame::setLearningRate(wxCommandEvent& event)
{
    wxString rateString;
    rateString << apcontrol->getLearningRate();
    bool valid = false;
    double newRate;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Learning Rate:"),
                wxT("Set Learning Rate"), rateString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newRateString = numberDialog.GetValue();
        valid = newRateString.ToDouble(&newRate) &&
                newRate >= 0 && newRate <= 1;
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a double between 0 and 1."),
                    wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setLearningRate(newRate);
}

void APFrame::setDiscountRate(wxCommandEvent& event)
{
    wxString rateString;
    rateString << apcontrol->getDiscountRate();
    bool valid = false;
    double newRate;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Discount Rate:"),
                wxT("Set Discount Rate"), rateString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newRateString = numberDialog.GetValue();
        valid = newRateString.ToDouble(&newRate) &&
                newRate >= 0 && newRate <= 1;
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a double between 0 and 1."),
                    wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setDiscountRate(newRate);
}

void APFrame::setRegularisationFactor(wxCommandEvent& event)
{
    if (!apcontrol->isNetLearner())
    {
        wxMessageDialog error(this,
                wxT("Only Net Learners have regularisation factors."),
                wxT("Wrong Learner Type"));
        error.ShowModal();
        return;
    }
    wxString regularisationFactorString;
    regularisationFactorString << apcontrol->getRegularisationFactor();
    bool valid = false;
    double newRegularisationFactor;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Regurlarisation Factor:"),
                wxT("Set Regularisation Factor"), regularisationFactorString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newRegularisationFactorString = numberDialog.GetValue();
        valid = newRegularisationFactorString.ToDouble(&newRegularisationFactor) &&
                newRegularisationFactor >= 0 && newRegularisationFactor <= 1;
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a double between 0 and 1."),
                    wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setRegularisationFactor(newRegularisationFactor);
}

void APFrame::setLambda(wxCommandEvent& event)
{
    if (!apcontrol->isTDNetLearner())
    {
        wxMessageDialog error(this,
                wxT("Only TD Net Learners have a prediction discount rate."),
                wxT("Wrong Learner Type"));
        error.ShowModal();
        return;
    }
    wxString lambdaString;
    lambdaString << apcontrol->getLambda();
    bool valid = false;
    double newLambda;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Prediction discount rate:"),
                wxT("Set Prediction Discount Rate"), lambdaString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newLambdaString = numberDialog.GetValue();
        valid = newLambdaString.ToDouble(&newLambda) &&
                newLambda >= 0 && newLambda <= 1;
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a double between 0 and 1."),
                    wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setLambda(newLambda);
}

void APFrame::setInputRate(wxCommandEvent& event)
{
    wxString periodString;
    periodString << apcontrol->getSamplePeriod();
    bool valid = false;
    unsigned int newPeriod;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Input Sample Period (ms):"),
                wxT("Set Input Sample Period in Milliseconds"), periodString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newPeriodString = numberDialog.GetValue();
        unsigned long num;
        valid = newPeriodString.ToULong(&num);
        if (valid)
        {
            newPeriod = (unsigned int)num;
            valid &= newPeriod > 0 and newPeriod == num;
        }
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a positive unsigned integer."),
            wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setSamplePeriod(newPeriod);
}

void APFrame::setLearningInstancesPerInstruction(wxCommandEvent& event)
{
    wxString instancesString;
    instancesString << apcontrol->getLearningInstancesPerInstruction();
    bool valid = false;
    unsigned int newInstances;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this,
                wxT("Learning Instances Per Instruction:"),
                wxT("Set Learning Instances"), instancesString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newInstancesString = numberDialog.GetValue();
        unsigned long num;
        valid = newInstancesString.ToULong(&num);
        if (valid)
        {
            newInstances = (unsigned int)num;
            valid &= newInstances > 0 && newInstances == num;
        }
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a positive unsigned integer."),
            wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setLearningInstancesPerInstruction(newInstances);
}

void APFrame::setLearningTranches(wxCommandEvent& event)
{
    wxString tranchesString;
    tranchesString << apcontrol->getLearningTranches();
    bool valid = false;
    unsigned int newTranches;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Learning Tranches:"),
                wxT("Set Learning Tranches"), tranchesString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newTranchesString = numberDialog.GetValue();
        unsigned long num;
        valid = newTranchesString.ToULong(&num);
        if (valid)
        {
            newTranches = (unsigned int)num;
            valid &= newTranches > 0 && newTranches == num;
        }
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a positive unsigned integer."),
            wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setLearningTranches(newTranches);
}

void APFrame::setLearningShowTime(wxCommandEvent& event)
{
    wxString showTimeString;
    showTimeString << apcontrol->getLearningShowTime();
    bool valid = false;
    unsigned int newShowTime;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Learning Show Time (s):"),
                wxT("Set Learning ShowTime"), showTimeString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newShowTimeString = numberDialog.GetValue();
        unsigned long num;
        valid = newShowTimeString.ToULong(&num);
        if (valid)
        {
            newShowTime = (unsigned int)num;
            valid &= newShowTime > 0 && newShowTime == num;
        }
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a positive unsigned integer."),
            wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setLearningShowTime(newShowTime);
}

void APFrame::setDisplayPackets(wxCommandEvent& event)
{
    apcontrol->displayPackets(event.IsChecked());
}

void APFrame::configureIMUs(wxCommandEvent& event)
{
    bool learningPaused = apcontrol->isLearningPaused();
    apcontrol->pauseLearning(true);
    apcontrol->setStatusMessage("Setting IMUs!");
    ma::IMUConfig dialog(this, apcontrol->getDataCollector(true));
    dialog.ShowModal();
    apcontrol->returnDataCollector();
    apcontrol->recordIMUSettings(true);
    apcontrol->setStatusMessage(ma::Str());
    apcontrol->pauseLearning(learningPaused);
}

void APFrame::loadInstructions(wxCommandEvent& event)
{
    wxFileDialog loadFileDialog(this, wxT("Load Instructions"),
            wxEmptyString, wxT("Instructions.txt"),
            wxFileSelectorDefaultWildcardStr,
            wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (loadFileDialog.ShowModal() == wxID_CANCEL) return;
    if (!apcontrol->loadInstructions(loadFileDialog.GetPath()))
        wxMessageBox(wxT("Load Failed"));
}

void APFrame::saveInstructions(wxCommandEvent& event)
{
    wxFileDialog saveFileDialog(this, wxT("Save Instructions"),
            wxEmptyString, wxT("Instructions.txt"),
            wxFileSelectorDefaultWildcardStr,
            wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
    if (!apcontrol->saveInstructions(saveFileDialog.GetPath()))
        wxMessageBox(wxT("Save Failed"));
}

void APFrame::editInstructions(wxCommandEvent& event)
{
    ma::Str instructionsText;
    unsigned int instructions = apcontrol->getInstructions();
    for (unsigned int instruction = 0; instruction < instructions; ++instruction)
    {
        instructionsText += apcontrol->getInstruction(instruction) + "\n";
    }
    ma::TextEditorDialog instructionsDialog(this, wxID_ANY, "Edit Instructions",
            instructionsText);
    if (instructionsDialog.ShowModal() == wxID_CANCEL) return;
    apcontrol->setInstructions(instructionsDialog.getText());
}

void APFrame::setPlaybackFile(wxCommandEvent& event)
{
    wxString filename = apcontrol->getPlaybackFilename();
    wxString windowTitle("Set Playback File.");
    wxFileDialog setFileDialog(this, windowTitle, wxEmptyString,
            filename, wxFileSelectorDefaultWildcardStr,
            wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (setFileDialog.ShowModal() == wxID_CANCEL) return;
    apcontrol->setPlaybackFilename(setFileDialog.GetPath());
}

void APFrame::setRealtime(wxCommandEvent& event)
{
    apcontrol->setRealtime(event.IsChecked());
}

void APFrame::loopPlayback(wxCommandEvent& event)
{
    if (!event.IsChecked())
    {
        apcontrol->loopPlayback(false);
        return;
    }
    wxString filename = apcontrol->getRecordFilename();
    wxString windowTitle("Set Record File.");
    wxNumberEntryDialog setLoopCountDialog(this,
            wxT("Enter the number of times to loop the playback.\n"
            "Use -1 to loop continuously."), wxEmptyString,
            wxT("Enter loop count"),
            apcontrol->getLoopPlaybackCount(), -1, 10000);
    if (setLoopCountDialog.ShowModal() == wxID_CANCEL) return;
    int loopCount = setLoopCountDialog.GetValue();
    wxMenu* menu = dynamic_cast<wxMenu*>(event.GetEventObject());
    if (menu) menu->Check(event.GetId(), loopCount != 0);
    apcontrol->loopPlayback(loopCount != 0);
    apcontrol->setLoopPlaybackCount(loopCount);
}

void APFrame::setRecordFile(wxCommandEvent& event)
{
    wxString filename = apcontrol->getRecordFilename();
    wxString windowTitle("Set Record File.");
    wxFileDialog setFileDialog(this, windowTitle, wxEmptyString,
            filename, wxFileSelectorDefaultWildcardStr,
            wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    if (setFileDialog.ShowModal() == wxID_CANCEL) return;
    apcontrol->setRecordFilename(setFileDialog.GetPath());
}

void APFrame::setPredictionLogFile(wxCommandEvent& event)
{
    wxString filename = apcontrol->getPredictionLogFilename();
    wxString windowTitle("Set Record File.");
    wxFileDialog setFileDialog(this, windowTitle, wxEmptyString,
            filename, wxFileSelectorDefaultWildcardStr,
            wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    if (setFileDialog.ShowModal() == wxID_CANCEL) return;
    apcontrol->setPredictionLogFilename(setFileDialog.GetPath());
}

void APFrame::controlMouse(wxCommandEvent& event)
{
    apcontrol->controlMouse(event.IsChecked());
}

void APFrame::setMouseSensitivity(wxCommandEvent& event)
{
    wxString periodString;
    periodString << apcontrol->getMouseSensitivity();
    bool valid = false;
    unsigned int newPeriod;
    while (!valid)
    {
        wxTextEntryDialog numberDialog (this, wxT("Delay between samples (ms):"),
                wxT("Set mouse sensitivity"), periodString);
        if (numberDialog.ShowModal() == wxID_CANCEL) return;
        wxString newPeriodString = numberDialog.GetValue();
        unsigned long num;
        valid = newPeriodString.ToULong(&num);
        if (valid)
        {
            newPeriod = (unsigned int)num;
            valid &= newPeriod > 0 && newPeriod == num;
        }
        if (!valid)
        {
            wxMessageDialog error (this,
                    wxT("Must be a positive unsigned integer."),
            wxT("Invalid value!"));
            error.ShowModal();
        }
    }
    apcontrol->setMouseSensitivity(newPeriod);
}

void APFrame::pauseLearningToolClicked(wxCommandEvent& event)
{
    bool learningPaused = apcontrol->isLearningPaused();
    if (learningPaused)
    {
        // Remove and store continue tool button.
        int position = toolBar->GetToolPos(ID_CONTINUE_LEARN);
        continueLearnTool = toolBar->RemoveTool(ID_CONTINUE_LEARN);
        // Insert and forget pause tool button.
        toolBar->InsertTool(position, pauseLearnTool);
        pauseLearnTool = 0;
    }
    else
    {
        // Remove and store pause tool button.
        int position = toolBar->GetToolPos(ID_PAUSE_LEARN);
        pauseLearnTool = toolBar->RemoveTool(ID_PAUSE_LEARN);
        // Insert and forget continue tool button.
        toolBar->InsertTool(position, continueLearnTool);
        continueLearnTool = 0;
    }
    toolBar->Realize();
    apcontrol->pauseLearning(!learningPaused);
}

void APFrame::learnToolClicked(wxCommandEvent& event)
{
    bool learning = apcontrol->isLearning();
    if (learning)
    {
        // Remove and store predict tool button.
        int position = toolBar->GetToolPos(ID_PREDICT);
        predictTool = toolBar->RemoveTool(ID_PREDICT);
        // Insert and forget learn tool button.
        toolBar->InsertTool(position, learnTool);
        learnTool = 0;
    }
    else
    {
        // Remove and store learn tool button.
        int position = toolBar->GetToolPos(ID_LEARN);
        learnTool = toolBar->RemoveTool(ID_LEARN);
        // Insert and forget predict tool button.
        toolBar->InsertTool(position, predictTool);
        predictTool = 0;
    }
    toolBar->Realize();
    apcontrol->learn(!learning);
}

void APFrame::recordToolClicked(wxCommandEvent& event)
{
    // Tell controller to start recording.
    apcontrol->startRecording();
    if (apcontrol->isRecording())
    {
        // Disable buttons.
        toolBar->EnableTool(ID_RECORD, false);
        toolBar->EnableTool(ID_START_PLAYBACK, false);

        // Enable stop button.
        toolBar->EnableTool(ID_STOP, true);

        apcontrol->startRecording();

        toolBar->Realize();
    }
    else
    {
        wxMessageDialog error (this, wxT("Control Error."),
        wxT("Unable to start recording!"));
        error.ShowModal();
    }
}

void APFrame::startPlaybackClicked(wxCommandEvent& event)
{
    // Remove and store playback start button.
    int position = toolBar->GetToolPos(ID_START_PLAYBACK);
    playbackStartTool = toolBar->RemoveTool(ID_START_PLAYBACK);
    // Insert and forget playback pause button.
    toolBar->InsertTool(position, playbackPauseTool);
    playbackPauseTool = 0;

    if (!apcontrol->isPlaybackPaused())
    {
        // Disable record button.
        toolBar->EnableTool(ID_RECORD, false);
        // Enable stop button.
        toolBar->EnableTool(ID_STOP, true);
    }

    toolBar->Realize();
    apcontrol->startPlayback();
}

void APFrame::pausePlaybackClicked(wxCommandEvent& event)
{
    // Remove and store playback pause button.
    int position = toolBar->GetToolPos(ID_PAUSE_PLAYBACK);
    playbackPauseTool = toolBar->RemoveTool(ID_PAUSE_PLAYBACK);
    // Insert and forget playback start button.
    toolBar->InsertTool(position, playbackStartTool);
    playbackStartTool = 0;

    toolBar->Realize();
    apcontrol->pausePlayback();
}

void APFrame::stopToolClicked(wxCommandEvent& event)
{
    // Disable stop button.
    toolBar->EnableTool(ID_STOP, false);
    // Enable record button.
    toolBar->EnableTool(ID_RECORD, true);
    if (apcontrol->isRecording())
    {
        // Tell controller to stop recording.
        apcontrol->stopRecording();
        if (!apcontrol->isRecording())
        {
            // Enable Start Playback button.
            toolBar->EnableTool(ID_START_PLAYBACK, true);
            toolBar->Realize();
        }
        else
        {
            wxMessageDialog error (this, wxT("Control Error."),
            wxT("Unable to stop recording!"));
            error.ShowModal();
        }
    }
    else if (apcontrol->isPlaybackPlaying())
    {
        if (!apcontrol->isPlaybackPaused())
        {
            // Remove and store playback pause button.
            int position = toolBar->GetToolPos(ID_PAUSE_PLAYBACK);
            playbackPauseTool = toolBar->RemoveTool(ID_PAUSE_PLAYBACK);
            // Insert and forget playback start button.
            toolBar->InsertTool(position, playbackStartTool);
            playbackStartTool = 0;
        }
        toolBar->Realize();
        apcontrol->stopPlayback();
    }
    else
    {
        wxMessageDialog error (this, wxT("Control Error."),
        wxT("Not recording or playing!"));
        error.ShowModal();
    }
}

void APFrame::quit(wxCommandEvent& event)
{
    Close(false);
}

void getNetworkDetails(wxWindow* parent, ma::Vector& layerSize,
        ma::NeuronType& neuronType)
{
    bool gotHiddenLayerSizes = false;
    while (!gotHiddenLayerSizes)
    {
        gotHiddenLayerSizes = getHiddenLayerSizes(parent, layerSize);
        if (!gotHiddenLayerSizes || layerSize.size() == 0)
        {
            gotHiddenLayerSizes = false;
            wxMessageDialog defaultNetworkDialog(parent,
                    wxT("Use default network?"),
                    wxT("Use Default Network"),
                    wxYES_NO | wxCANCEL);
            int returnValue = defaultNetworkDialog.ShowModal();
            if (returnValue == wxID_CANCEL) return;
            if (returnValue == wxID_YES)
            {
                gotHiddenLayerSizes = true;
                layerSize = ma::Vector();
            }
        }
    }
    bool gotHiddenNeuronType = false;
    while (layerSize.size() > 2 && !gotHiddenNeuronType)
    {
        if (!getHiddenNeuronType(parent, neuronType))
        {
            wxMessageDialog defaultNeuronTypeDialog(parent,
                    wxT("Use default neuron type?"),
                    wxT("Use Default Neuron Type"),
                    wxYES_NO | wxCANCEL);
            int returnValue = defaultNeuronTypeDialog.ShowModal();
            if (returnValue == wxID_CANCEL) return;
            if (returnValue == wxID_YES)
            {
                gotHiddenNeuronType = true;
                neuronType = ma::NeuronType::Logistic;
            }
        }
    }
}

bool getHiddenLayerSizes(wxWindow* parent, ma::Vector& layerSize)
{
    unsigned int layers = (unsigned int)-1;
    while (layers == (unsigned int)-1)
    {
        wxTextEntryDialog layerDialog(parent,
                wxT("Enter number of hidden layers"));
        layerDialog.SetTextValidator(wxFILTER_DIGITS);
        if (layerDialog.ShowModal() == wxID_CANCEL) return false;
        layers = wxAtoi(layerDialog.GetValue()) + 2;
    }
    ma::Vector newLayerSize(layers);
    for (unsigned int layer = 1; layer < layers - 1; ++layer)
    {
        while (newLayerSize[layer] == 0)
        {
            wxString text = wxT("Enter number of neurons in hidden layer ");
            text << layer;
            wxTextEntryDialog layerDialog(parent, text);
            layerDialog.SetTextValidator(wxFILTER_DIGITS);
            if (layerDialog.ShowModal() == wxID_CANCEL) return false;
            newLayerSize[layer] = wxAtoi(layerDialog.GetValue());
        }
    }
    swap(layerSize, newLayerSize);
    return true;
}

bool getHiddenNeuronType(wxWindow* parent, ma::NeuronType& neuronType)
{
    wxString choice[2];
    choice[0] = wxT("Logistic");
    choice[1] = wxT("Threshold");
    wxSingleChoiceDialog neuronTypeDialog(parent,
            wxT("Select the hidden layers neuron type"),
            wxT("Hidden Layers Neuron Type"), 2, choice);
    neuronTypeDialog.SetSelection(0);
    neuronTypeDialog.SetSize(330, 220);
    if (neuronTypeDialog.ShowModal() == wxID_CANCEL) return false;
    switch (neuronTypeDialog.GetSelection())
    {
    case 0:
        neuronType = ma::NeuronType::Logistic;
        return true;
    case 1:
        neuronType = ma::NeuronType::Threshold;
        return true;
    default:
        return false;
    }
}
