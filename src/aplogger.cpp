// (c) 2016: Marcel Admiraal

#include "aplogger.h"

APLogger::APLogger(const char* filename)
{
    setLogFilename(ma::Str(filename));
}

APLogger::~APLogger()
{
}

void APLogger::currentInstruction(const char* instruction)
{
    ma::Str logLine = "Current Instruction: ";
    logLine << instruction;
    log(logLine);
}

void APLogger::startLearning()
{
    log("Start Learning");
}

void APLogger::stopLearning()
{
    log("Stop Learning");
}

void APLogger::sampleData()
{
    log("Sample Data");
}

void APLogger::sequenceReset()
{
    log("Sequence Reset");
}

void APLogger::actualResult(const unsigned int resultIndex)
{
    ma::Str logLine = "Actual Result: ";
    logLine << ma::toStr(resultIndex);
    log(logLine);
}

void APLogger::recordIMUSettings()
{
    log("Recording IMU Settings");
}

void APLogger::recordingPredictionMetaData()
{
    log("Recording prediction meta data");
}

void APLogger::recordingPrediction()
{
    log("Recording prediction");
}

void APLogger::addingBlankLineToPredicitionLogFile()
{
    log("Adding blank line to prediction log file");
}
