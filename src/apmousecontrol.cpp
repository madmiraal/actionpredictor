// (c) 2016: Marcel Admiraal

#include "apmousecontrol.h"

#include <wx/utils.h>

#include "apcontrol.h"
#include "vector.h"

MouseControl::MouseControl(APControl* control) :
    milliSecondDelay(50), running(true), controllingMouse(false),
    control(control)
{
    CreateThread();
    GetThread()->Run();
}

MouseControl::~MouseControl()
{
    running = false;
    GetThread()->Wait();
}

unsigned int MouseControl::getSensitivity() const
{
    return milliSecondDelay;
}

void MouseControl::setSensitivity(const unsigned int milliSecondDelay)
{
    this->milliSecondDelay = milliSecondDelay;
}

void MouseControl::controlMouse(bool controllingMouse)
{
    this->controllingMouse = controllingMouse;
}

wxThread::ExitCode MouseControl::Entry()
{
    while (running)
    {
        if (controllingMouse)
        {
            ma::Vector prediction = ma::softMax(control->getPrediction());
            double trigger = 1.0 / prediction.size();
            if (prediction.size() >= 2)
            {
                int x = 0;
                if (prediction[0] > trigger)
                    x += (int)(-(prediction[0] - trigger) * 100);
                if (prediction[1] > trigger)
                    x += (int)((prediction[1] - trigger) * 100);
                pointer.moveX(x);
            }
            if (prediction.size() >= 4)
            {
                int y = 0;
                if (prediction[2] > trigger)
                    y += (int)(-(prediction[2] - trigger) * 100);
                if (prediction[3] > trigger)
                    y += (int)((prediction[3] - trigger) * 100);
                pointer.moveY(y);
            }
        }
        wxMilliSleep(milliSecondDelay);
    }
    return (wxThread::ExitCode)0;
}
