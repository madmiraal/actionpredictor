// (c) 2016: Marcel Admiraal

#include "apapp.h"

#include "apframe.h"

IMPLEMENT_APP(APApp);

bool APApp::OnInit()
{
    APFrame* frame = new APFrame();
    frame->Show();
    return true;
}
