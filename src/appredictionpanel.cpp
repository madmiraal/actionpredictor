// (c) 2016: Marcel Admiraal

#include "appredictionpanel.h"

#include <wx/dcbuffer.h>
#include <wx/colour.h>

#include "vector.h"
#include "fittextpanel.h"
#include "wxfactory.h"

#include <cmath>

APPredictionPanel::APPredictionPanel(wxWindow* parent, APControl* control) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxFULL_REPAINT_ON_RESIZE),
        control(control)
{
    Bind(wxEVT_ERASE_BACKGROUND, &APPredictionPanel::stopErase, this);
    Bind(wxEVT_IDLE, &APPredictionPanel::onIdle, this);
    Bind(wxEVT_PAINT, &APPredictionPanel::paint, this);
}

APPredictionPanel::~APPredictionPanel()
{
}

void APPredictionPanel::stopErase(wxEraseEvent& event)
{
    // Intercepting erase event to stop erasing.
}

void APPredictionPanel::onIdle(wxIdleEvent& evt)
{
    Refresh();
}

void APPredictionPanel::paint(wxPaintEvent& event)
{
    // Create buffered paint device context.
    ma::Vector prediction = control->getPrediction();
    unsigned int length = prediction.size();
    if (length == 0) return;
    if (length == 1)
    {
        ma::Vector expand(2);
        expand[0] = 1 - prediction[0];
        expand[1] = prediction[0];
        prediction = expand;
        length = 2;
    }

    wxSize panelSize = GetSize();
    unsigned int panelWidth = panelSize.GetWidth();
    unsigned int panelHeight = panelSize.GetHeight();
    unsigned int predictionHeight = (panelHeight * 0.9) / 2;
    unsigned int colourHeight = (panelHeight * 0.9) / 2;
    wxBitmap buffer(panelSize);
    wxBufferedPaintDC dc(this, buffer);
    unsigned int red, green, blue;

    // Draw prediction.
    unsigned int maxIndex = prediction.maxIndex();
    // Set background colour.
    setBackgroundColour(red, green, blue, prediction[maxIndex]);
    dc.SetBrush(wxColour(red, green, blue));
    // Draw background box.
    dc.DrawRectangle(0, 0, panelWidth, predictionHeight);
    // Set text colour based on background colour.
    if (red + green + blue > 255 * 1.5)
        dc.SetTextForeground(*wxBLACK);
    else
        dc.SetTextForeground(*wxWHITE);
    // Fit text into box.
    wxString instruction(control->getInstruction(maxIndex).c_str());
    instruction << " (" << (unsigned int)(round(prediction[maxIndex] * 100)) << "%)";
    unsigned int pointSize = ma::getBestFitFontSize(
            this, instruction, panelWidth, predictionHeight);
    wxFont font = dc.GetFont();
    font.SetPointSize(pointSize);
    dc.SetFont(font);
    wxSize textSize = dc.GetTextExtent(instruction);
    dc.DrawText(instruction, (panelWidth - textSize.GetWidth()) / 2,
            (predictionHeight - textSize.GetHeight()) / 2);

    // Draw colour boxes.
    unsigned int start = 0;
    for (unsigned int index = 0; index < length; ++index)
    {
        unsigned int thisWidth = panelWidth * prediction[index];
        // Correct for rounding error.
        while (index == length-1 && start + thisWidth < panelWidth) ++thisWidth;
        // Set background colour based on confidence.
        setBackgroundColour(red, green, blue, prediction[index]);
        dc.SetBrush(wxColour(red, green, blue));
        // Draw background box with size based on confidence.
        dc.DrawRectangle(start, predictionHeight, thisWidth, colourHeight);
        // Set text colour based on background colour.
        if (red + green + blue > 255 * 1.5)
            dc.SetTextForeground(*wxBLACK);
        else
            dc.SetTextForeground(*wxWHITE);
        // Fit text into box.
        wxString instruction(control->getInstruction(index).c_str());
        unsigned int pointSize = ma::getBestFitFontSize(
                this, instruction, thisWidth, colourHeight);
        wxFont font = dc.GetFont();
        font.SetPointSize(pointSize);
        dc.SetFont(font);
        wxSize textSize = dc.GetTextExtent(instruction);
        dc.DrawText(instruction,
                start + ((thisWidth - textSize.GetWidth()) / 2),
                predictionHeight + ((colourHeight - textSize.GetHeight()) / 2));

        start += thisWidth;
    }

    // Draw text boxes.
    start = 0;
    unsigned int boxWidth = panelWidth / length;
    unsigned int boxHeight = panelHeight - predictionHeight - colourHeight;
    for (unsigned int index = 0; index < length; ++index)
    {
        // Draw background box.
        dc.SetPen(*wxBLACK);
        dc.SetBrush(*wxLIGHT_GREY);
        dc.DrawRectangle(start, predictionHeight + colourHeight,
                boxWidth, boxHeight);
        // Enter text.
        dc.SetTextForeground(*wxBLACK);
        wxString percentage;
        percentage << (unsigned int)(round(prediction[index] * 100)) << '%';
        unsigned int pointSize = ma::getBestFitFontSize(
                this, percentage, boxWidth, boxHeight);
        wxFont font = dc.GetFont();
        font.SetPointSize(pointSize);
        dc.SetFont(font);
        wxSize textSize = dc.GetTextExtent(percentage);
        dc.DrawText(percentage,
                start + ((boxWidth - textSize.GetWidth()) / 2),
                predictionHeight + colourHeight +
                (boxHeight - textSize.GetHeight()) / 2);

        start += boxWidth;
    }
}

void APPredictionPanel::setBackgroundColour(unsigned int& red,
        unsigned int& green, unsigned int& blue, const double confidence)
{
    if (confidence <= 0.5)
    {
        red = 255;
        green = confidence * 2 * 255;
        blue = 0;
    }
    else
    {
        red = (1 - (confidence - 0.5)) * 2 * 255;
        green = 255;
        blue = 0;
    }
}
