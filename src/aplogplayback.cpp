// (c) 2016: Marcel Admiraal

#include "aplogplayback.h"

#include "apcontrol.h"

#include "strstream.h"

APLogPlayback::APLogPlayback(APControl* control, const ma::Str& filename) :
    IMULogPlayback(control->getDataCollector()), control(control)
{
    setLogFilename(filename);
}

APLogPlayback::~APLogPlayback()
{
}

void APLogPlayback::startPlayback()
{
    bool wasPaused = isPaused();
    LogPlayback::startPlayback();
    if (!isPlaying())
        control->setStatusMessage("Playback file not found.");
    else if (wasPaused && !isPaused())
        control->setStatusMessage("Playback continuing...");
    else
        control->setStatusMessage("Playback stated.");
}

void APLogPlayback::pausePlayback()
{
    LogPlayback::pausePlayback();
    if (isPaused())
        control->setStatusMessage("Playback paused.");
}

void APLogPlayback::stopPlayback()
{
    LogPlayback::stopPlayback();
    control->setStatusMessage(ma::Str());
}

bool APLogPlayback::processEntry(const ma::Str& entry)
{
    if (!ma::IMULogPlayback::processEntry(entry))
    {
        if (entry.left(21) == "Current Instruction: ")
            currentInstruction(entry.trimLeft(21));
        else if (entry == "Start Learning")
            startLearning();
        else if (entry == "Stop Learning")
            stopLearning();
        else if (entry == "Sample Data")
            sampleData();
        else if (entry == "Sequence Reset")
            sequenceReset();
        else if (entry.left(15) == "Actual Result: ")
            actualResult(entry.trimLeft(15));
        else if (entry == "Recording IMU Settings")
            recordIMUSettings();
        else if (entry == "Recording prediction meta data")
            recordPredictionMetaData();
        else if (entry == "Recording prediction")
            recordPrediction();
        else if (entry == "Record delayed prediction")
            recordDelayedPrediction();
        else if (entry == "Adding blank line to prediction log file")
            addBlankLineToPredicitionLogFile();
        else
            return false;
    }
    return true;
}

void APLogPlayback::logFileStart()
{
    ma::IMULogPlayback::logFileStart();
    control->setStatusMessage("Playback started.");
}

void APLogPlayback::logFileEnd()
{
    ma::IMULogPlayback::logFileEnd();
    control->setStatusMessage("Playback finished.");
}

void APLogPlayback::currentInstruction(const ma::Str& instruction)
{
    control->setCurrentInstruction(instruction);
}

void APLogPlayback::startLearning()
{
    control->setStatusMessage("Learning.");
    control->startPredictorLearning();
}

void APLogPlayback::stopLearning()
{
    control->setStatusMessage("");
    control->stopPredictorLearning();
}

void APLogPlayback::sampleData()
{
    control->tellPredictorToSampleData();
}

void APLogPlayback::sequenceReset()
{
    control->tellPredictorSequenceReset();
}

void APLogPlayback::actualResult(const ma::Str& resultString)
{
    ma::StrStream resultStream(resultString);
    unsigned int result = (unsigned int)-1;
    resultStream >> result;
    if (result != (unsigned int)-1)
    {
        control->tellPredictorActualResult(result);
    }
    else
    {
        std::cerr << "Expected unsigned integer, but got: " << resultString;
        std::cerr << std::endl;
    }
}

void APLogPlayback::recordIMUSettings()
{
    control->recordIMUSettings();
}

void APLogPlayback::recordPredictionMetaData()
{
    control->setStatusMessage("Recording predictions.");
    control->recordPredictionMetaData();
}

void APLogPlayback::recordPrediction()
{
    unsigned int historyLength = control->getDataCollector()->getHistoryLength();
    if (historyLength / 2 > 0)
    {
        insertDelayedEntry(std::chrono::milliseconds(historyLength / 2),
                "Record delayed prediction");
    }
    else
    {
        control->recordPrediction();
    }
}

void APLogPlayback::recordDelayedPrediction()
{
    control->recordPrediction();
}

void APLogPlayback::addBlankLineToPredicitionLogFile()
{
    control->setStatusMessage(ma::Str());
    control->addBlankLineToPredicitionLogFile();
}
